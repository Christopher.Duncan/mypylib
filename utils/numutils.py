import numpy as np

deg2rad = 0.017453292519943295
arcsec2deg = 0.0002777777777777778

def pythLineDistance(x,y):
    """
    Return the pythagorean line distance between x and y. x and y must be conformal in the final dimension
    """
    x = np.array(x); y = np.array(y)
    if(x.shape[-1] != y.shape[-1]):
        raise ValueError("mypylib:utils:numutils:lineDistance: x and y not conformal in last dimension")
    return np.sqrt(x*x + y*y)

def polarAngle2D(x,y):
    """
    Return the polar angle (counterclockwise to x-axis) of a line connecting points x and y on a 2D plane
    """
    x = np.array(x); y = np.array(y)
    return np.arctan(y/x)
