import ggLens
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from . import NFW
import distances

ang2phys = 1000*np.pi/(180.*3600.) # Convert arcsec --> kpc
dist = distances.Distance()
ri, rf = 200, 4000 # inner, outer radii in arcsec
gglnbins = 5
zlens = 0.165  # typical lens redshift

h = 0.7

# Create NFW halo object - Testing on A901a size object Heymans 2008
nfw_halo = NFW.NFWhalo(z=zlens, Mvir=18.8e13/h)
nfw_halo_rvir = NFW.NFWhalo(z=zlens, Rvir = 1.194/h)

r_as = np.linspace(ri,rf,1000) # In arcsec
r_Mpc = (r_as*dist.angular_diameter_distance(zlens)*ang2phys)/1000.

plt.ylabel('shear', fontsize='x-large')
plt.xlabel('r (arcsec)', fontsize='x-large')

# Plot also the shear expected from the NFW halo
plt.plot(r_as, nfw_halo.Shear(r_Mpc, zs=0.8), 'k-')
plt.plot(r_as, nfw_halo_rvir.Shear(r_Mpc, zs=0.8), 'g-')

plt.grid()
#plt.legend(numpoints=1)
plt.show()

