
def isiterable(a):
    return None

def swap(a,b):
    """
    Swap a and b through and intermediary
    """

    return b, a


def deep_getsizeof(o, ids):
    """
    Found at: https://github.com/the-gigi/deep/blob/master/deeper.py

    Find the memory footprint of a Python object
    This is a recursive function that rills down a Python object graph
    like a dictionary holding nested dictionaries with lists of lists
    and tuples and sets.
    The sys.getsizeof function does a shallow size of only. It counts each
    object inside a container as pointer only regardless of how big it
    really is.
    :param o: the object
    :param ids:
    :return:
    """
    d = deep_getsizeof
    if id(o) in ids:
        print("deep_getsizeof: retuning 0 as represented")
        return 0

    r = getsizeof(o)
    ids.add(id(o))

    if isinstance(o, str) or isinstance(0, str):
        return r

    if isinstance(o, Mapping):
        return r + sum(d(k, ids) + d(v, ids) for k, v in o.items())

    if isinstance(o, Container):
        return r + sum(d(x, ids) for x in o)

    return r


def astropy_table_to_numpy(tab):
    """
    Convert astropy table elements to numpy array, where every element is described by the type of the first element
    WARNING: the dtype of all table elements must be the same for this to work
    :param tab: Astropy table instance
    :return: Numpy array of dtype equal to the first element of the table, with dimensions (nrows, ncols).
    """

    arr = tab.as_array()
    return arr.view(arr[0][0].dtype).reshape(len(tab), -1)