import numpy as np

def fisher_matrix(lnL, mlPoint, args = [], **kwargs):
    """
    Calculate an estimate of the hessian (fisher matrix when applied to multiple realisations) by taking the second derivative of the lnL function across all parameters

    :param args: List of arguements (beyond parameter value as first arguement), required by lnLike evaluation function
    :param kwargs: Dictionary of keyword arguments required by lnL evaluation
    """
    nDim = len(mlPoint)

    import mypylib.numutil.derivative as myder
    import copy

    if not "h" in kwargs:
        kwargs["h"] = 1.e-4

    FM = np.zeros((nDim,nDim))
    for i in range(nDim):
        for j in range(nDim):
            if(i==j):
                FM[i,i] = myder.df_converge(lnL, i, copy.copy(mlPoint), n = 2, args = args, **kwargs)
                #FM[i,i] = myder.partial_derivative(lnL, i, copy.copy(mlPoint), n = 2, args = args, h = h,**kwargs)
            else:
                FM[i,j] = myder.ddf(lnL, [i,j], copy.copy(mlPoint), args, **kwargs)
                FM[j,i] = FM[i,j]
            
    FM *= -1
    return FM
    
def get_distribution_moment(x,f, order):
    """
    Get the order-th moment for the given distribution

    NOTE: This does not attempt to renormalise the distribution, and assumes the distribution is constructed on it's full support.
    """
    
    xx = np.prod(np.tile(x,(order,1)), axis = 0) #This should return array of ones, size x, if order = 0

    return np.trapz(xx*f, x = x)

def normalise_distribution(x,f):
    """

    NOTE: This assumes the distribution is constructed on it's full support.
    """
    return f/get_distribution_moment(x,f,0)

def get_distribution_mean(x,f):
    """
    Return the mean of the distribution as M = int [dx x*f]. 

    NOTE: This does not attempt to renormalise the distribution, and assumes the distribution is constructed on it's full support.
    """
    return get_distribution_moment(x,f,1)

def get_distribution_variance(x,f):
    """
    Return the variance of the distribution, as <x**2>-mu**2

    NOTE: This does not attempt to renormalise the distribution, and assumes the distribution is constructed on it's full support.
    """

    meansq = get_distribution_moment(x,f,2)
    mean = get_distribution_moment(x,f,1)

    
    return meansq - mean*mean


def normal_distribution(x, mean = 0, sigma = 0,
                        lower_limit = None,
                        upper_limit = None):

    chi2 = np.power((x-mean)/sigma,2)

    A = 1. / (np.sqrt(2. * np.pi) * sigma)
    N = A*np.exp(-0.5*chi2)

    if lower_limit is None and upper_limit is None:
        return N

    from scipy.special import erf
    isoutside = np.zeros(x.shape, dtype = bool)

    Y1 = 1
    if lower_limit is not None:
        isoutside = np.logical_or(isoutside, x<lower_limit)

        Y1 = (mean-lower_limit)/(np.sqrt(2)*sigma)
        Y1 = erf(Y1)

    Y2 = -1.
    if upper_limit is not None:
        isoutside = np.logical_or(isoutside, x>upper_limit)

        Y2 = (mean-upper_limit)/(np.sqrt(2)*sigma)
        Y2 = erf(Y2)

    norm = 0.5*(Y1-Y2)

    isInvalid = norm <= 0.
    """
    if isInvalid.sum() > 0:
        print("Invalid normalisation for:", x[isInvalid], isInvalid.sum(), x.shape)
        print(" Mean: ", mean[isInvalid])
        print(" with upper limit: ", upper_limit)
        print(" mean -upper: ", mean-upper_limit)
        print("erf arg: ", (mean-upper_limit)/(np.sqrt(2)*sigma))
        #print("with Y1: ", Y1[isInvalid])
        print("and Y2: ", Y2[isInvalid])
        print(" And sigma:", sigma[isInvalid])
        print("Norm is", norm[isInvalid])
        input("Check")
    """
    # This isn't strictly correct, but can happen where the mean is:
    #    >> upper
    #    << lower
    # or lower > upper (and vice-versa)
    norm[isInvalid] = 1.e100

    N[isoutside] = 1.e-100

    #print("isoutside: ", isoutside.sum())
    #print("isinvalid: ", isInvalid.sum())

    # Renormalise to PDF
    N /= norm

    return N


def schecter_analytic_normalisation(alpha, lower = None, upper = None):
    from scipy.special import gamma

    assert lower is None and upper is None, "schecter_analytic_normalisation: Not yet" \
                                            "implemented for lower and upper limits. " \
                                            "Should use incomplete gamma function"

    assert alpha > -1, "MyPyLib: stats: schecter distribution: renormalisation with gamma only works if alpha > -1." \
                       " Alpha entered: %f" % alpha
    norm = 0.9210340371976184 / gamma(alpha + 1)  # 0 to infinity, and only for alpha > -1

    return norm

def schecter_distribution(x, xstar = 0, alpha = 0, renorm = True, lower = None, upper = None):
    """
    Return the Schecter distribution, commonly used to describe the magnitude distribution of galaxies
    :param x:
    :param xstar:
    :param alpha:
    :return:
    """
    from scipy.special import gamma

    exp1 = -0.4*(x-xstar)
    exp2 = exp1*(alpha+1)

    if renorm:
        norm = schecter_analytic_normalisation(alpha, lower, upper)
    else:
        norm = 1.

    dist = norm*np.power(10., exp2)*np.exp(-1.*np.power(10.,exp1))
        
    if lower is not None:
        selection = x<=lower
        dist[selection] = 0.
    if upper is not None:
        selection =  x>upper
        dist[selection] = 0.
        
    return dist
