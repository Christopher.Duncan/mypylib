

class MassProfile(object):
    """
    Base class containing the shared attributes of the mass profile
    """
    def __init__(self, centroid = None, redshift = None, **kwargs):
        #Use of kwargs means that invalid parameters can be ignored
        self.centroid = centroid
        self.z = redshift


    def update(self, **kwargs):
        if('centroid' in kwargs):
            self.centroid = kwargs['centroid']
        if('redshift' in kwargs):
            self.x = kwargs['redshift']
