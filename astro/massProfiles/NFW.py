"""Class for a halo with an NFW profile
The first major attribute is the shear from an NFW, other attributes
will follow.

##########
Edits:
cajd (2Mar2014): Added Dolag et al 2004 mass-concentration relation (for SH project and comparison to Heymans 2008. On initialisation, NFW object defaults to this mass-concetration relation
               : Edited intialisation so that NFW can be specified by virial radius rather than virial mass.
#########

NOTE: Where radiusDefn = 2 The virial radius here is defined as in Wright and Brainerd, where r_vir is set in comparison to the **critical density**. To use case where r_vir set to *mean matter density* (DEFAULT), (Mvir = Mvir*Omega_M(z) ), set radiusDefn = 1


TODO: Tidy up parameter definition. At the minute, parameter values are set by use of the update routine, and other set accordingly using setProfile. This needs to be tidied up
"""

import numpy
import mypylib.astro.distances as distances
from . import GeneralProfile
import sys

__author__ = "Ami Choi <amichoi@gmail.com>, Christopher Duncan <christopher.duncan@physics.ox.ac.uk>"
from math import pi
G = 4.3E-9 # Mpc/Msun (km/s)**2
c = 299792. # (km/s)
h = 0.7

from mypylib.astro.astrolib import angular_diameter_distance
from mypylib.utils.numutils import deg2rad, pythLineDistance

class NFWhalo(GeneralProfile.MassProfile):
    """
    General features of an NFW halo include:
        - Mass
        - Size
        - Concentration
        - Redshift

    The user can only specify two properties, the redshift and Mvir, and can
        also choose a mass-concentration relation from Maccio (default !!DEFUNCT!!) or
        Duffy, or Dolag 2004 (default cajd Edit)
    """
    def __init__(self,z = None,Mvir=None,Rvir=None,cosmo=None,cMethod=None, c = None, radiusDefn=None,
                 separation_units = "mpc",
                 source_position_type = "sep",
                 *args, **kws):
        ##cajd Edit: Mvir in MSun, RVir in Mpc
        """
        Initialisation for the NFWhalo class. Constructed as defined in Heymans 2008

        :param z: Redshift of the profile. If none, defaults to 0.3
        :param MVir: Mass of the profile in [MSun]
        :param RVir: Virial radius in [Mpc]
        :param cosmo: Cosmology class instance (see astro.distances module)
        :param cMethod: Mass-Concentration relation to use. For 3: Dolag et al 2004. 0 allows concentration to be set by hand.
        :param c: Concentration value. Only used if cMethod == 0
        :param radiusDefn: How the radius is defined. For 1: In comparison to the critical density,
            as in Wright and Brainerd. For 2: In comparison to mean matter density.
        :param separation_units: Units in which profile - lens
        :param source_position_type:
            "pos" - position (needs corrected for central.)
             "sep" - separation, already corrected for central
        """

        ##Initialise BaseClass
        super(NFWhalo, self).__init__(*args, **kws)
        #GeneralProfile.MassProfile.__init__(self, centroid = centroid, redshift = z)
        
        if z is None:
            self.z = 0.3
        else:
            self.z = z

        self.Mvir = Mvir
        self.Rvir = Rvir
        
        if(Mvir is None and Rvir is None):
            raise ValueError("astro:massProfiles:NFW: Either RVir or MVir must be initialised")
        
        if cosmo is None:
            self.cosmo = distances.Distance()
        else:
            self.cosmo = distances.Distance(cosmo)
        if cMethod is None or cMethod>2 or cMethod<0:
            ##Default Mass Concentration Method - Dolag 2004 (cajd Edit)
            print('--NFW initialisation: Default c(M) relation is Dolag 2004')
            self.cMethod = 3
        else:
            self.cMethod = cMethod
        if radiusDefn is None:
            print('--NFW initialisation: Using definition of r_vir wrt mean matter density by default')
            self.radiusDefn = 1
        else:
            self.radiusDefn = radiusDefn

        if(self.cMethod == 0):
            if c is None:
                raise ValueError("NFW: init: cMethod is 0 but no valid concentration was set")
            self.c = c

        # Save the angular diameter distance to the lens to save time later
        self.D_L = angular_diameter_distance(self.z)

        # Save seperation determination
        self._src_pos_units = separation_units.lower()
        assert self._src_pos_units == "mpc" or self._src_pos_units == "deg", "NFW: Only MpC or Degree separation " \
                                                                             "supported"
        self._src_pos_type = source_position_type.lower()
        assert self._src_pos_type == "pos" or self._src_pos_type == "sep", "NFW: Only pos or sep position type " \
                                                                           "supported"

        self.Da = None
        self.delta = None
        self.Delta = None
        self.setProfile()

    def update(self, **kwargs):
        """
        Update the model parameters, and re-initialise the halo instance.

        NOTE: There is a lot of duplication with this and init. It may be a good idea to combine these.
        """
        #self.__init__(*args, **kwargs) #This updates all, of sets any not in kwargs to default

        #There must be a nore general way to do this
        if('z' in kwargs):
            self.z = kwargs['z']
        if('Mvir' in kwargs):
            self.Mvir = kwargs['MVir']
            self.Rvir = None
        if('Rvir' in kwargs):
            self.Rvir = kwargs['Rvir']
            self.Mvir = None
            self.logMvir = None
            if("Mvir" in kwargs):
                raise ValueError("NFW: update: Both Mvir and Rvir entered. Only one should be entered")
        if('cosmo' in kwargs):
            self.cosmo = distances.Distance(kwargs['cosmo'])
        if('cMethod' in kwargs):
            self.cMethod = kwargs['cMethod']
        if('radiusDefn' in kwargs):
            self.radiusDefn = kwargs['radiusDefn']
        if('centroid' in kwargs):
            self.centroid = kwargs['centroid']
        if "centroid_x" in kwargs:
            self.centroid[0] = kwargs["centroid_x"]
        if "centroid_y" in kwargs:
            self.centroid[1] = kwargs["centroid_y"]

        #Concentration
        if('c' in kwargs):
            self.c = kwargs['c']
            
        self.Da = None
        self.delta = None
        self.Delta = None
        self.setProfile()

        
    def setProfile(self):
        """
        Set all definitions for the profile. This is called after update, so is the main routine for keeping track of updates
        """
        self.rhoCrit = self.get_rhoCrit()
        if(self.radiusDefn == 1): ##Mean Matter Density
            rhoc0 = (self.cosmo.OMEGA_M + self.cosmo.OMEGA_L)*(self.cosmo.h*1e2)**2
            rhoc0 = 3.*rhoc0/(8.*pi*G)
            
            omega_m = ( rhoc0*self.cosmo.OMEGA_M* ((1.+self.z)**3.) )/ self.rhoCrit
        elif(self.radiusDefn == 2):## Critical Density
            omega_m = 1.
        else:
            raise ValueError('NFW: Critical Error in setting rhoCrit by radiusDefn')
        self.rhoCrit = self.rhoCrit*omega_m

        self.Da = self.cosmo.Da(self.z)/206265.
        #self.Delta = self.get_Delta()
        self.Delta = 200.

        ##Get virial radius/ virial mass
        if(self.Rvir is None and self.Mvir is None):
            print('NFW: Either Rvir or Mvir MUST BE SET')
        elif(self.Rvir is None):## MVir set
            self.logMvir = numpy.log10(self.Mvir)
            self.Rvir = self.get_Rvir()
        elif(self.Mvir is None):##r200 Set
            self.Mvir = self.get_Mvir()
            self.logMvir = numpy.log10(self.Mvir)
        #else:
            # print "RVir: ", self.Rvir
            # print "MVir: ", self.Mvir
            # raise ValueError("NFW: setProfile: Error setting RVir and MVir")
        #print self.Delta

        if(self.cMethod != 0):
            #Update concentration from mass-concentration relation. If cMethod is zero, it is assumed that the current concentration value is valid
            self.c = self.get_c() ##Needs Mvir set
            
        self.delta = self.get_delta()
        
    def get_c(self):
        """
        Return the concentration using a mass-concentration relation, defined by the stored cMethod switch.


        cMethod == 1: 
        cMethod == 2: Duffy 2008 [WMAP5] Equation 4
        cMethod == 3:
        """
        
        if self.cMethod==1:
            conc = 0.971 - numpy.log10(self.cosmo.OMEGA_L+self.cosmo.OMEGA_M*(1.+self.z)**3)/3. - 0.094*numpy.log10(self.cosmo.h/1e12)
            return 10**(conc - 0.094*self.logMvir)
        elif(self.cMethod == 2): ##cajd Edit: Duffy 2008 [WMAP5] Equation 4: Fit Parameters Table 1, Row 2 NFW.
            #return 7.85*(self.Mvir/2e12)**-0.081/(1.+self.z)**0.71
            Mscale = 2.E12/h
            return 5.71*(self.Mvir/Mscale)**-0.084/(1.+self.z)**0.47
        elif(self.cMethod == 3): ##Dolag et al 2004: Mvir *must* bu in Msun/h
            return 9.59/(1.+self.z)*(((self.Mvir/h)/10.E14)**(-0.102))
        else:
            raise ValueError("NFW:get_c: Invalid cMethod value (if zero, then this should not have been called) cMethod: %d"%(int(self.cMethod)))

    def _convert_separation(self, r):

        # Adjust for centroid if necessary
        sep = numpy.asarray(r)

        #print("Converting separation!", self._src_pos_type)

        if self._src_pos_type == "pos":

            #print("Pos type")

            #print("Min/Max on entry:", numpy.min(r[:,0]), numpy.max(r[:,0]), numpy.min(r[:,1]), numpy.max(r[:,1]))
            #print("centroid is: ", self.centroid)

            assert self._src_pos_units.lower() == "deg", "Only degree separation supported for pos type"
            assert r.shape[1] == 2, "If position is entered, the r array must be 2D, with second dimension length 2"
            sep = (sep - self.centroid) # ngal, 2
            
            # Convert to an actual separation
            sep = pythLineDistance(sep[:,0], sep[:,1])

            #print("sep: ", numpy.min(sep), numpy.max(sep))
            #input("check")

        if self._src_pos_units == "deg":
            # Convert to Mpc
            sep *= self.D_L*deg2rad
            
        return sep

    def get_Delta(self):
        """ from Bryan & Norman """
        X = -self.cosmo.OMEGA_L/(self.cosmo.OMEGA_M*(1.+self.z)**3 + self.cosmo.OMEGA_L)
        #return 18*pi**2 + 82.*X - 39.*X**2
        return 200  # For M200 instead of Mvir

    def get_delta(self):
        return self.Delta*self.c**3/(numpy.log(1.+self.c)-self.c/(1.+self.c))/3.

    def get_rhoCrit(self):
        """ Returns rho_crit in Msun/Mpc^3 """
        H2 = (self.cosmo.OMEGA_M*(1.+self.z)**3 + self.cosmo.OMEGA_L)*(self.cosmo.h*1e2)**2
        return 3.*H2/(8.*pi*G)

    def get_Rvir(self):
        """ Rvir in Mpc """
        r = numpy.log10(4.*pi*self.rhoCrit*self.Delta/3.)/-3.
        return 10**(r+self.logMvir/3.)

    def get_Mvir(self):
        return self.Delta*self.rhoCrit*(4.*pi/3.)*(self.Rvir**3.)
        
    def set_Mvir(self,Mvir):
        self.Mvir = Mvir
        self.logMvir = numpy.log10(Mvir)
        self.c = self.get_c()
        self.delta = self.get_delta()
        self.Rvir = self.get_Rvir()

    def set_c(self, c = None):
        """
        Setter for the concentration parameter.

        :param c: Float. The concentration of the halo. If None, then c will be set using a mass-concentration relation according to cMethod
        """

        if(c is None):
            self.c = self.get_c()
        else:
            self.c = c

        self.delta = self.get_delta()
        
            
    def set_z(self,z):
        self.z = z

        self.D_L = angular_diameter_distance(self.z)

        self.setProfile()

    def set_cMethod(self,cMethod):
        if cMethod>0 and cMethod<3:
            self.cMethod = cMethod


            self.set_c(c = None)
            #DEPRECATED for set_c
            #self.c = self.get_c()
            #self.delta = self.get_delta()


    def _atanfunc(self,x0):
        """
        General atan-like functions for NFW profile. See equations 15 and 16 of Wright and Brainerd 2000
        """
        res = x0*0.
        x = x0[x0>1].copy()
        res[x0>1] = numpy.arctan(((x-1.)/(x+1.))**0.5)/(x**2-1.)**0.5
        x = x0[x0<1].copy()
        res[x0<1] = numpy.arctanh(((1.-x)/(1.+x))**0.5)/(1.-x**2)**0.5
        return res

    def _radialDepShear(self,x):
        """
        Radial dependence of NFW shear, as defined in equations 14, 15, 16 in Wright and Brainerd 2000

        :param x: Normalised radius (r/r_s, with r_s = rVir/c)
        """
        result = x*0.
        x0 = x[x!=1]
        result[x!=1] = 8*self._atanfunc(x0)/x0**2 + 4*numpy.log(x0/2.)/x0**2 - 2./(x0**2-1.) + 4*self._atanfunc(x0)/(x0**2-1.)
        result[x==1.] = 0.5607446 # 10/3 + 4 ln 1/2
        return result

    def _radialDepConv(self, x):
        """
        Radial dependence of the surface mass density, as given is equation 11 of Wright and Brainerd 2000

        :param x: Normalised radius (r/r_s, with r_s = rVir/c)
        """
        result = x*0.

        x0 = x[x!=1]
        result[x!=1] = (1.-2.*self._atanfunc(x0))/(x0**2 - 1.)
        result[x==1] = 0.333333333333333333333333
        
        return result
        
    def DeltaSigma(self,r):
        """
        Get the differential surface mass density for radius r (can be array)
        """

        r = self._convert_separation(r)

        #Get normalised radius, x = r/rs, with rs = rVir/c
        x = r*self.c/self.Rvir
        
        norm = self.delta*self.Rvir*self.rhoCrit/self.c
        return norm*self._radialDepShear(x)


    def Sigma(self, r):
        """
        Return the surface mass density for radius r
        """

        r = self._convert_separation(r)

        #Get normalised radius, x = r/rs, with rs = rVir/c
        rs = self.Rvir/self.c
        x = r/rs

        norm = 2.*self.delta*self.rhoCrit*rs

        return norm*self._radialDepConv(x)
    
    def Mass_NFW(self,r):

        r = self._convert_separation(r)

        x = r*self.c/self.Rvir
        norm = 4*numpy.pi*self.rhoCrit*self.delta*(self.Rvir/self.c)**3
        return norm*(numpy.log(1+x)-1./(1+x))

    def SigmaCrit(self,zs=None,beta=None):
        if zs is not None:
            if type(zs)==type([]):
                zs = numpy.array(zs)
            if type(zs)==type(numpy.empty(0)): #If numpy array
                beta = zs*0.
                for i in range(zs.size):
                    if(zs[i] < self.z):
                        beta[i] = 1.e-200
                    else:
                        beta[i] = self.cosmo.Da(self.z,zs[i])/self.cosmo.Da(zs[i])
            else:
                if(zs < self.z):
                    beta = 1.e-200
                else:
                    beta = self.cosmo.Da(self.z,zs)/self.cosmo.Da(zs)
        else:
            beta = numpy.asarray(beta)

        sCrit = c**2/(4*pi*G*self.Da*beta*206265.) # This 206265 undoes the division by this on contruction
        # of Da, and is actually uneccesary (as long as removed in both cases).
        
        return sCrit

    def Shear(self,r,zs=None,beta=None):
        if zs is None and beta is None:
            print("Must set zs or beta!")
            return
        sigCrit = self.SigmaCrit(zs,beta)
        #r = numpy.asarray(r)
        
        return self.DeltaSigma(r)/sigCrit

    def Convergence(self, r, zs = None, beta = None):
        if zs is None and beta is None:
            print("Must set zs or beta!")
            return
        sigCrit = self.SigmaCrit(zs,beta)
        #r = numpy.asarray(r)
        return self.Sigma(r)/sigCrit

    def lensingQuantities(self, r, shear = False, reducedshear = False, conv = False, magnification = False,
                          zs = None, beta = None, sigCrit = None):
        if(sigCrit is None):
            if zs is None and beta is None:
                print("Must set zs or beta!")
                return
            sigCrit = self.SigmaCrit(zs,beta)
    
        #r = numpy.asarray(r)

        sigma = None; dSigma = None
        gamma, kappa = None, None
        if(shear or magnification or reducedshear):
            #Get differential SMD
            dSigma = self.DeltaSigma(r)
            gamma = dSigma/sigCrit

        if(reducedshear or conv or magnification):
            #Get SMD
            sigma = self.Sigma(r)
            kappa = sigma/sigCrit
            
        res = []
        if(shear):
            res.append(gamma)
        if(reducedshear):
            res.append(gamma/(1.-kappa))
        if(conv):
            res.append(kappa)
        if(magnification):
            res.append(1./((1.-kappa)**2 - gamma**2))

        if(len(res) == 0):
            raise RunTimeError("NFW:NFWhalo:lensingQuantities: No measure requested")
            
        return res
