"""
This module contains helper classes for general matplotlib setup
"""

import matplotlib as mpl
import pylab as pl

#################### Style ########################

def mplstyle_publication():

    mpl.rc('font', family='Franklin Gothic Book',size = 18)
    mpl.rc('text', usetex=True)
    mpl.rc('xtick', labelsize=15)
    mpl.rc('ytick', labelsize=15)
    mpl.rc('axes', labelsize=16)
    mpl.rc("legend", fontsize = 12)
    mpl.rcParams['lines.linewidth'] = 2.0

    mpl.rcParams.update({'axes.titlesize': 20})


################### Figure Size #######################

#def mpl_subplot_border(f,left = 0.15, right = 0.9, bottom = 0.15, top = 0.9):
#    f.subplots_adjust(left = left, right = right, bottom = bottom, top = top)
#    return f

def mplfig_golden(width = 3.1, f = None):
    if f is None:
        f = mpl.figure()


    height = width / 1.618

    f.set_size_inches(width, height)

    return f

def mplfig_square(width = 3.1, f= None):
    if f is None:
        f = pl.figure()

    f.set_size_inches(width, width)

    return f

def set_color_cycle(color_list=None):
    """

    :param color_list: List/Tuple or instance of routine which returns color list
    :return:
    """
    try:
        color_list = color_list()
    except:
        pass

    assert isinstance(color_list, (list, tuple)), "Color list must be a list of tuple"

    mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=color_list)

def color_list_1():
    """
    As taken from https://towardsdatascience.com/making-matplotlib-beautiful-by-default-d0d41e3534fd
    :return:
    """
    CB91_Blue = '#2CBDFE'
    CB91_Green = '#47DBCD'
    CB91_Pink = '#F3A0F2'
    CB91_Purple = '#9D2EC5'
    CB91_Violet = '#661D98'
    CB91_Amber = '#F5B14C'
    return [CB91_Blue, CB91_Pink, CB91_Green, CB91_Amber,
              CB91_Purple, CB91_Violet]