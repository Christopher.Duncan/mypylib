import numpy as np

def linearInterp_regularGrid(xw, xmin, dx, z):

    i0 = ((xw-xmin)//dx).astype(int)
    i1 = i0 + 1

    isIn = np.logical_and(i0 >=0, i1 < z.shape[0])
    bottom = i0 < 0
    top = i1 > z.shape[0]

    x0 = i0[isIn]*dx
    x1 = x0+dx

    z0 = z[i0[isIn]]
    z1 = z[i1[isIn]]

    res = np.empty_like(xw)
    res[isIn] = z0 + (xw[isIn]-x0)*((z1-z0)/(x1-x0))

    if isIn.sum() == res.shape[0]: return res

    res[top] = z[-1]
    res[bottom] = z[0]

    return res

def bilinearInterp_regularGrid(xw, yw,
                               xmin, dx,
                               ymin, dy,
                               z):

        """
        2D linear interpolation function, where input is in 64-bit floats. In all values, "x" corresponds to first
        dimension, "y" to second (i.e. for 2D array z = z[x,y]). Array to be interpolated (z) is assumed to by tabulated
        on a regular grid, starting from xmin and ymin, with spacing dx, dy.

        :param xw: Array of wanted x values (i.e. requested values for xw in first dimension). Each element constitutes
            a pair with ymin (e.g. the N'th requested value is at xw[N], yw[N]
        :param yw: As xw, for y (second dimension)
        :param xmin: Minimum x-value on which z is tabulated.
        :param dx: Spacing of sampling for z in x.
        :param ymin: As xmin, for y.
        :param dy: As dx, for y.
        :param z: Tabulated value to be interpolated on xw, yw. z[x,y].

        :returns 1D numpy array of float64, includoing tabulated values for all xw,yw pairs.
        """


        zd1 = z.shape[0]
        zd2 = z.shape[1]
        xd1 = xw.shape[0]
        res = np.zeros([xd1])

        xii0 = ((xw-xmin)//dx).astype(int); xii1 = xii0 + 1
        yii0 = ((yw-ymin)//dy).astype(int); yii1 = yii0 + 1

        left = xii0 < 0
        right = xii1 >= zd1
        bottom = yii0 < 0
        top = yii1 >= zd2

        isIn = np.logical_and(np.logical_and(np.logical_and(xii0 >= 0,yii0 >= 0), xii1 < zd1), yii1 < zd2)
        #isOut = ~isIn

        ixii0 = xii0[isIn]
        ixii1 = xii1[isIn]
        iyii0 = yii0[isIn]
        iyii1 = yii1[isIn]

        iyw = yw[isIn]
        ixw = xw[isIn]

        x0 = xmin + ixii0*dx
        x1 = x0 + dx
        y0 = ymin + iyii0*dy
        y1 = y0 + dy


        w0 = (x1 - ixw) * (y1 - iyw)
        w1 = (x1 - ixw) * (iyw - y0)
        w2 = (ixw - x0) * (y1 - iyw)
        w3 = (ixw - x0) * (iyw - y0)

        res[isIn] = (w0 * z[ixii0, iyii0] + w1 * z[ixii0, iyii1] + w2 * z[ixii1, iyii0] + w3 * z[ixii1, iyii1]) / (dx * dy)

        if isIn.sum() == xw.shape[0]: return res

        if np.any(bottom):
            res[bottom] = linearInterp_regularGrid(xw[bottom], xmin, dx, z[0,:])
        if np.any(top):
            res[top] = linearInterp_regularGrid(xw[top], xmin, dx, z[-1,:])
        if np.any(left):
            res[left] = linearInterp_regularGrid(yw[left], ymin, dy, z[:,0])
        if np.any(right):
            res[right] = linearInterp_regularGrid(yw[right], ymin, dy, z[:,1])

        return res
