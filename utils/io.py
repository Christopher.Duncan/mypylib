import numpy as np
import os
import h5py
import astropy.io.fits as pyfits

def mkdirs(filepath):
    """
    Starting from base, iteratively construct directory structure directory-by-directory, test for existence, and make is required
    """
    
    import os
    
    dire = os.path.dirname(filepath).split('/')

    tdire = ''
    for sdire in dire:
        tdire = os.path.join(tdire, sdire)
        if(not os.path.exists(tdire)):
            os.mkdir(tdire)
            
def exist_or_mkdir(filepath):
    """
    Check for exisitence of directory, and if does not exist, make the directory. Directory is constructe drecursively using mkdirs()
    """
    import os

    if not os.path.exists(os.path.dirname(filepath)):
        mkdirs(filepath)

    return 1


#---- FITS ROUTINES
def fits_search_header(path, cards = None, hIndex = 0):
    """
    Return as a dictionary all cards listed in cards, which exist in FITS file at path, in HDU hIndex

    :param path: Full path to FITS file
    :param cards: DEFAULT None. List of cards to search for in header. If None, then all cards in header are returned.
    :param hIndex: DEFAULT 0 (primary). The index which labels which HDU ti query

    :returns Dictionary of all cards in header
    """
    
    hdulist = pyfits.open(path)

    head = hdulist[hIndex].header

    if(cards is None):
        return dict(head)
    else:
        dic = dict()
        for card in cards:
            if card in head:
                dic[card] = head[card]

        return dic

def output_images_to_MEF(filename, data, **header):
    """
    Outputs the images stored in list [data] to a multi-extension fits
    :param data:
    :param header:
    :return:
    """

    hdulist = pyfits.HDUList()

    priHDU = pyfits.PrimaryHDU()
    hdulist.append(priHDU)
    counter = 0
    for dta in data:
        counter += 1
        hdulist.append(pyfits.ImageHDU(data = dta))

    for key, val in header.items():
        hdulist[0].header.set(key,val)

    hdulist.writeto(filename, clobber = True)
    print("Output MEF to: %s \n "%(filename))
    print("Total number of elements was ", counter)


def output_images_to_MEF2(filename, data, **header):
    """
    Outputs the images stored in list [data] to a multi-extension fits
    :param data:
    :param header:
    :return:
    """

    #priHDU = pyfits.PrimaryHDU()
    #listhdu = [priHDU]
    #for dta in data:
    #    listhdu.append(pyfits.ImageHDU(dta))

    print("Called output to MEF!")
    print(len(data))

    priHDU = pyfits.PrimaryHDU()
    hdul = [priHDU]
    for i in range(len(data)):
        assert isinstance(data[i], (list, tuple, np.ndarray)), "output_images_to_MEF2: Data must be list, tuple or array"
        imHDU = pyfits.ImageHDU(data[i])
        hdul.append(imHDU)

    hdulist = pyfits.HDUList(hdul)

    #hdulist = pyfits.HDUList(listhdu)

    hdulist.writeto(filename, clobber = True)
    print("Output MEF to: %s \n "%(filename))


def append_image_to_MEF(filename, data, **header):
    """
    Append the data to a fits MEF file. Likely to be slower, but allows greater control over the the sources, especially 
    in the case where teh amount of data is likely to be an issue on RAM (i.e. cann append, delete from RAM and then read in 
    using memMap
    """

    
    # If it does not exist, then create it.
    if not os.path.isfile(filename):
        priHDU = pyfits.PrimaryHDU()

        listHDU = [priHDU]
        listHDU = pyfits.HDUList(listHDU)

        listHDU.writeto(filename, clobber = True)

    # Append onto MEF
    pyfits.append(filename, data = data)
        
    
#---- H5PY ROUTINES
def save_dict_to_hdf5(filename, dic):
    """
    .... https://codereview.stackexchange.com/questions/120802/recursively-save-python-dictionaries-to-hdf5-files-using-h5py
    """
    exist_or_mkdir(filename)
    
    with h5py.File(filename, 'w') as h5file:
        _recursively_save_dict_contents_to_group(h5file, '/', dic)

def _recursively_save_dict_contents_to_group(h5file, path, dic):
    """
    .... https://codereview.stackexchange.com/questions/120802/recursively-save-python-dictionaries-to-hdf5-files-using-h5py
    """
    for key, item in list(dic.items()):
        if isinstance(item, (np.ndarray, np.int64, np.float64, str, bytes, int, float)):
            h5file[path + key] = item
        elif isinstance(item, dict):
            _recursively_save_dict_contents_to_group(h5file, path + key + '/', item)
        elif isinstance(item, (list, tuple)):
            # Attempt conversion to numpy array for output
            h5file[path + key] = np.array(item)
        elif item is None:
            h5file[path + key] = "None"
        elif isinstance(item, np.ndarray) and str(item.dtype)[1] == "U":
            length = str(item.dtype)[2:] # Unicode dtype is "<UXX" where XX is the length
            h5file[path+key] = item.astype("|S"+length)
            continue
        else:
            raise ValueError('Cannot save %s%s type'%(key,type(item)))

def load_dict_from_hdf5(filename):
    """
    ....https://codereview.stackexchange.com/questions/120802/recursively-save-python-dictionaries-to-hdf5-files-using-h5py
    """
    with h5py.File(filename, 'r') as h5file:
        return _recursively_load_dict_contents_from_group(h5file, '/')

def _recursively_load_dict_contents_from_group(h5file, path):
    """
    .... https://codereview.stackexchange.com/questions/120802/recursively-save-python-dictionaries-to-hdf5-files-using-h5py
    """
    ans = {}
    for key, item in list(h5file[path].items()):
        if isinstance(item, h5py._hl.dataset.Dataset):
            ans[key] = item[()]#item.value
        elif isinstance(item, h5py._hl.group.Group):
            ans[key] = _recursively_load_dict_contents_from_group(h5file, path + key + '/')
    return ans
