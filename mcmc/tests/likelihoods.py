import numpy as np


def logL_prodGauss(x, Args = None):
    return -(0.5/2.)*(x-10.)**2 -(0.5/2.)*(x-14.)**2

#Addition of two Guassians
def logL_sumGauss(x, Args = None):
    from scipy.stats import norm
    return np.log(norm.pdf(x,loc = 9., scale = .3) + norm.pdf(x, loc = 15., scale = .3))

def logL_sumGauss_2d(x, Args = None):
    from scipy.stats import multivariate_normal as norm
    return np.log(norm.pdf(x,mean = (9.,9.), cov = .3) + norm.pdf(x, mean = (15.,15.), cov = .3))
