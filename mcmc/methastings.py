import numpy as np
from scipy import random
import math

class gaussianProposal:
    def __init__(self, var, nPar = 1, nDrawStore = 1000):
        
        self.mean = 0.
        self.var = var

        self.symmetric = True

        
        self.nQuery = 0
        self.nPar = nPar
        self.nDrawStore = nDrawStore

        self.drawStore = None
        self._redraw()
        
    def _redraw(self):
        """
        Redraw self.nDrawStore random numbers for the proposal distribution, for each parameter. These are stored.
        """
        self.drawStore = random.normal(self.mean, self.var,  self.nPar*self.nDrawStore).reshape((self.nPar, self.nDrawStore))
        
    def propose(self,x):
        """
        Draw from the proposal distribution to propose the next step, starting from point x
        """
        if(self.nQuery >= self.drawStore.shape[1]):
            self._redraw()
            self.nQuery = 0
        ret = self.drawStore[:,self.nQuery]
        #Shift by current value (as guassian)
        ret += x
        self.nQuery += 1
        return ret

def gelman_rubin_convergence(chains):
    """
    Calculate the Gelman-Rubin statistics for multiple chains. The G-R statistic compares the posterior variance (given as a weighted mean of the between-chain variance and the mean within-chain variance) to the mean within chain variance: If this ratio is large, then the between-chain variance is much larger than the within-chain variance, and thus the chains are not probing the same stationary target distribution -> further runs are needed.

    :param chains: List of numpy arrays  where each element of the list is a seperate chain, all of the same size. Each chain must be of shape (nParamater, nLink)

    :return List of the gelman rubin statistic for each parameter
    """
    
    #Convert list to numpy chain
    chains = np.array(chains)

    M = float(chains.shape[0])
    P = float(chains.shape[1])
    n = float(chains.shape[2])
    
    ##Calculate between chain variance
    chainMean = chains.mean(axis = -1) #Shape (nChain, nPar)
    betweenChainMean = chainMean.mean(axis = 0) #Shape nPar
    diff= chainMean-betweenChainMean #Shape (nChain, nPar)
    
    B = M*(n/(M-1))*(diff*diff).mean(axis = 0)
    
    ##Calculate the within chain variance
    chainVar = chains.var(axis = -1, ddof = 1) #Shape (nChain, nPar)

    W = chainVar.mean(axis = 0)

    ##Calculate the posterior variance
    V = ((n-1)/n)*W + ((M+1.)/(n*M))*B

    return V/W #Shape nPar
    
class mhmcmc:
    def __init__(self,proposal, logL, startPoint, logLArgs = None):
        """
        The initialiser for the mhmcmc class.

        :param proposal: Class for the proposal distribution. Must contain members symmetric (labelling whether proposal is symmetric), propose(x) (ewhich returns a new proposed positions starting from x), and lnProbGiven(x,xl), which gives the log-Probability of x given xl, *only if not symmetric*.
        :param logL: Pointer to function which returns the log-Probability of the distribution to be sampled
        :param startPoint: The starting point for the free parameters in the chain. If a numpy array, the length of the array sets the number of parameters to vary, in the same order as that required by logL
        """
    
        #Initialise
        self.proposal = proposal
        self.logL = logL
        self.logLArgs = logLArgs
        
        ##Random number storage
        self._nChallenge = 0
        self._urand = None
        self._redrawUniform()

        ##Acceptance Statistics
        self._nAccept = 0
        self._nStep = 0

        ##Result Storage
        self._chain = None

        ##State storage
        self._current = startPoint
        
    def _redrawUniform(self):
        self._urand = random.uniform(size = 1000)
        
    def _proposeStep(self):
        """
        @brief Propose the next step
        @param x: The current position (across all parameters)
        """
        return self.proposal.propose(self._current)

    def _challengeStep(self,x, xl):
        alpha = self.logL(x,self.logLArgs) - self.logL(xl,self.logLArgs)

        #If the proposal is not symmetric, adjust for the p(xl|x)/p(x|xl) factor
        if(not self.proposal.symmetric):
            alpha += self.proposal.lnProbGiven(xl,x) - self.proposal.lnProbGiven(x,xl)


        if(alpha >= 0.):
            #print "Accepting by default : ", xl, " -> ", x
            
            return True
        else:
            if(self._nChallenge >= self._urand.shape[0]):
                self._redrawUniform()
                self._nChallenge = 0

            #Accept step with probability exp(alpha)
            u = self._urand[self._nChallenge]
            
            if(u<math.exp(alpha)):
                #print "Accepting by alpha : ", xl, " -> ", x, " ::: Alpha ::: ", math.exp(alpha), " :: u :: ", u
                ret = True
            else:
                #print "Rejecting by alpha : ", xl, " -> ", x, " ::: Alpha ::: ", math.exp(alpha), " :: u :: ", u
                ret = False
            self._nChallenge += 1
            return ret

    def step(self, burn = False):
        """
        Take a single step. If accepted (and not burned), the _chain will be extended, and acceptance rate adjusted

        :param burn: If true, step will be discarded independently of whether it was accepted or not 
        """
        
        xl = self._current
        x = self._proposeStep()
        accept = self._challengeStep(x,xl)

        if(not burn):
            self._nStep += 1
        if(accept):
            self._current = x
            if(not burn):
                self._nAccept += 1
                self._appendChain(x)
            ret = x
        else:
            if(not burn):
                self._appendChain(xl)
            ret =  None
        return ret
            
    def nSteps(self, numStep, nBurn = 0):
        """
        Take numStep steps. Burn the first nBurn
        """
        for i in range(numStep):
            self.step(burn = (i<nBurn))
        
    def _appendChain(self, x):
        """
        Append the chain link (x) to the chain
        """
        if(self._chain is None):
            self._chain = x.reshape((x.shape[0],1))
        else:
            self._chain = np.hstack((self._chain, x.reshape((x.shape[0],1))))
        
    ### Query the chain
    def acceptanceRate(self):
        """
        Return the acceptance rate
        """
        return self._nAccept/float(self._nStep)

    def rejectionRate(self):
        """
        Return the rejection rate

        """
        return 1.-(self.acceptanceRate())

    def getChain(self):
        """
        Return the chain itself

        :return The chain, as a 2D numpy array, first dimension labelling parameter, second chain link
        """
        return self._chain

    def chainMean(self):
        """
        Return the mean of the chain across all parameters

        :return The mean of all parameters in the chain, as a 1D numpy array
        """
        return self._chain.mean(axis = 1)

    def chainStD(self):
        """
        Return the standard deviation of the chain.
        
        :return The standard devidation of the chain for all parameters, as a 1D numpy array
        """
        return self._chain.std(axis = 1)


class mchain_mhmcmc:
    """
    Class for a multiple chain version of the mcmc, on which convergence criteria can be tested
    """
    def __init__(self, nChain, nConvTest, startPoints = None, initSupport = None, **MCMCArgs):
        self.nChain = nChain
        self.nConvTest = nConvTest

        self.startPoints = startPoints
        if(startPoints is None):
            #Select random start points on initSupport
            #initSupport should be of shape (nPar,2)
            if(initSupport is None):
                raise ValueError("methastings:mchain_mhmcmc: Either startpoints or initSupport must be supplied")
            if(initSupport.shape[1] != 2):
                raise ValueError("methastings:mchain_mhmcmc: initSupport axis 1 must be of length 2, defining the hard beginning and end of the parameter support")

            self._randomStart(initSupport)

        print(("Started with start points : ", self.startPoints))
               
        self.nPar = self.startPoints.shape[1]
            
        #Produce instances of each mcmc    
        self.mcmcs = []
        for i in range(nChain):
               MCMCArgs['startPoint'] = self.startPoints[i]
               self.mcmcs.append(mhmcmc(**MCMCArgs))
    

    def _randomStart(self,initSupport):
        """
        Randomly start the chains in the initSupport range for all parameters
        """
        #Generate nChain*nPar random uniforms
        nPar = initSupport.shape[0]
        _urand = random.uniform(size = self.nChain*nPar).reshape((self.nChain, nPar))

        print(("Called random start, with length: ", _urand.shape))
        
        self.startPoints = np.zeros(_urand.shape)
        for par in range(nPar):
            self.startPoints[:,par] = _urand[:,par]*(np.diff(initSupport[par,:])) + initSupport[par,0]

        print(("Start points are: ", self.startPoints))

    def run(self,nBurn = 0,maxStep = None, convCrit = 1.03):
        """
        Run all chains until the Gelman-Rubin convergence criteria is satisfied.

        :param nBurn: The number of initial samples to burn. DEFAULT: 0
        :param maxStep: The maximum number of steps to consider. If None, mcmc will progress indefinitely, or until convergence. DEFAULT: None
        :param convCrit: The convergence criteria. The convergence statistics will have to satisfy this for all parameters. DEFAULT: 1.03
        """
        grStat = 1000.*np.ones(self.nPar)

        tStep = 0
        while((grStat < convCrit).sum() != self.nPar):
            if(tStep == 0):
                nburn = nBurn
            else:
                nburn = 0
            for M in range(self.nChain):
                self.mcmcs[M].nSteps(numStep = self.nConvTest, nBurn = nburn)

            tStep += self.nConvTest

            #Don't get G-R stat if no acceptable steps
            if(tStep <= nBurn):
                continue
            
            grStat = gelman_rubin_convergence(self.getChain(collected = False))
            
            if((maxStep is not None) and (tStep >= maxStep)):
                print("WARNING: mchain_mhmcmc: Convergence not acheived in maxstep")
                break

            
    def getChain(self,collected = False):
        """
        Return the chains. If collected = False, the return has shape (nChain, nPar, nLink). If collected == True, the return has shape (nPar, nLink*nChain)
         """
        
        chains = np.array([self.mcmcs[M].getChain() for M in range(self.nChain)])
        if(collected):
            chains = np.hstack(chains)
        return chains


    def acceptanceRate(self):
        return np.array([self.mcmcs[M].acceptanceRate() for M in range(self.nChain)])
