"""
Module contianing routines for dictionary manipulation
"""

def cat_dicts(dicts):
    """
    Concatonate dictionaries dicts, by appending elements of each dict value onto the end of a list, which can then be converted into a numpy array as required.

    If keys are the same for all dicts, then all keys in concatonated dict will have the same length. However, if any key is not represented in all dictionaries, 
    then that key will be smaller than others. As such *where not all keys are shared, whilst the ordering is the same, the indexing of values in the concatonated dict
    will not match elements of the original dict*.

    :param dicts: A list of dictionaries to append
    :returns A dictionary with the same elements as input dicts, where each value is a list of each dictionaries values
    """

    res = dict()
    for DIC in dicts:
        for key, val in DIC.items():
            if key in res:
                #Append onto end
                res[key].append(val)
            else:
                #Initialise element
                res[key] = [val]

    return res
