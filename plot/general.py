import pylab as pl
import numpy as np

def set_axes_labels(ax, **kwargs):
    """
    Automatic setting of labels and titles on axes based on dictionary parsing
    """
    
    #Parse keyword arguements
    if('title' in kwargs):
        tit = kwargs['title']
    else:
        tit = ''
    if('xlabel' in kwargs):
        xlab = kwargs['xlabel']
    else:
        xlab = ''
    if('ylabel' in kwargs):
        ylab = kwargs['ylabel']
    else:
        ylab = ''

    if(xlab is not None):
        ax.set_xlabel(xlab)
    if(ylab is not None):
        ax.set_ylabel(ylab)
    if(tit is not None):
        ax.set_title(tit)

def abs_linePlot(x,y, ax = None, plotit = False, title = None, xlab = None, ylab = None, legLab = None, color = None):

    if(ax is None):
        ax = pl.subplot()

    set_axes_labels(ax,
                    xlabel = xlab,
                    ylabel = ylab,
                    title  = title)
        
    plotLegend = legLab is not None
        
    #Subdivide into sections and plot
    sec = np.zeros(x.shape, dtype = bool)
    isEnd = False
    lowerX = None #Stores lower x for transition
    for IX in range(len(y)):
        if(y[IX] > 0):
            signMod = +1
            ln = "-"
        else:
            signMod = -1
            ln = "--"

        sec[IX] = True

        isTransition = False
        if(IX == len(y)-1):
            isEnd = True

            pX = x[sec]
            pY = signMod*y[sec]

        elif(y[IX] == 0):
            isTransition = True
            
        elif(y[IX+1]/y[IX] < 0):
            isTransition = True

            pX = x[sec]
            pY = signMod*y[sec]

            #Find zero point and append onto end
            
        if(isEnd or isTransition):
            p = ax.plot(x[sec], signMod*y[sec], linestyle = ln, color = color, label = legLab)
            if(color is None):
                color = p[0].get_color()
            legLab = None
            
            sec[:] = False
            

    if(plotit):
        if(plotLegend):
            ax.legend(loc = 0)
        
        pl.show()
        
def multiplicate_additive_bias(x,y, marker = None, color = None, ax = None, plotit = True, title = None, xlab = None, ylab = None, legLab = ""):
    """
    Create a plot which shows multiplicative and additive bias in multiple measures of x and y.

    Plots x against y-x, and fits (y-x) = mx+c, or y = (1+m)x+c.

    :param x: Values to compare against (finds linear in y = (1+m)x + c
    :param y: Values to compare
    """

    
    if(ax is None):
        ax = pl.subplot()
        
    x = x.flatten()
    y = y.flatten()
    delta = y-x
    
    ax.scatter(x, delta, marker = "x")

    #Get linear regression
    from scipy.stats import linregress

    reg = linregress(x, delta)
    slope = reg[0]
    cept = reg[1]

    #Plot linear regression line
    xreg = np.linspace(x.min(), x.max(), 1000)
    yreg = slope*xreg + cept
    ax.plot(xreg, yreg, ls = "--", label = "%s m = %f c = %e"%(legLab,slope,cept))

    ax.legend(loc = 0)

    if(title is not None):
        ax.set_title(title)

    if(xlab is not None):
        ax.set_xlabel(xlab)
        if(ylab is not None):
            ax.set_ylabel(ylab+"-"+xlab)
        
    if(plotit):
        pl.show()
    
def errorBar(x, y, yerr = None, xerr = None, ax = None, plotit = False, **kwargs):
    """
    Produce and errorbar plot

    :param x: The x-axis grid evaluation points
    :param y: The y-axis values
    :param yerr: The error in the y-value
    :param yerr: The error in the x-value
    :param ax: The axes to plot on
    :param plotit: If true, plot will be output to screen
    :param kwargs: Extra arguments used for labels, titles etc
    """
    
    if(plotit or ax is None):
        f = pl.figure()
        ax = f.add_subplot(111)

    ax.errorbar(x, y, yerr = yerr, xerr = xerr, ls = 'none', marker = 'x')

    set_axes_labels(ax, **kwargs)

    if(plotit):
        pl.show()

def stickPlot(X,Y,e1,e2,rescale = 1., scale_length = 0.05, save_path = None):
    """
    Produce a stick plot for ellipticities e1, e2 on position x, y on the image
    :param X: X position of each stick to plot
    :param Y: Y position of each stick to plot
    :param e1: ellipticity component on X,Y axis for each X, Y position
    :param e2: ellipticity component at 45 degrees to X,Y axis for each X, Y position
    :param rescale: Rescale value for length of stick. When |e| = 1, the length of the stick = rescale in figure units (0,1)
    :param scale_length: The length of line to draw as a scale
    :return:
    """

    import mypylib.plot.mplSettings as mplpar
    mplpar.mplfig_square()
    mplpar.mplstyle_publication()

    modE = np.sqrt(np.power(e1,2) + np.power(e2,2))
    theta = 0.5*np.arctan2(e2, e1)

    #print("modE:", modE, e1, e2)
    #print(modE)
    #print(modE.max(), modE.min())

    # Rescale
    modE = modE*rescale

    #print("modE:", modE, rescale)

    # Convert back to U, V for matpplotlib
    U = modE*np.cos(theta)
    V = modE*np.sin(theta)

    # Alter X, Y to account for centre of arrow
    X = X - U/2
    Y = Y - V/2

    import pylab as pl
    f, ax  = pl.subplots()

    # Loop through all points and plot stick
    for x,y,u,v  in zip(X,Y,U,V):
        ax.plot([x, x+u], [y, y+v], color = "k")

    ax.set_xlabel("X (deg)")
    ax.set_ylabel("Y (deg)")

    from matplotlib.patches import FancyArrow 
    arrow_length = scale_length*rescale

    xlim = ax.get_xlim()
    x_width = np.diff(xlim)[0]

    ylim = ax.get_ylim()
    y_width = np.diff(ylim)[0]    

    # Aspect must the equal to avoid distortion of the length
    ax.set_aspect("equal")

    #p = FancyArrow(0.8, 1.05, arrow_length, 0.0, transform = ax.transAxes, clip_on = False, head_width = 0., width = 0.005, color = "k")
    #p = FancyArrow(xlim[0]+0.8*x_width, ylim[1]+0.05*y_width, arrow_length, 0.0, clip_on = False, head_width = 0., width = 0.005, color = "k")
    p = FancyArrow(xlim[0], ylim[1]+0.05*y_width, arrow_length, 0.0, clip_on = False, head_width = 0., width = 0.005, color = "k")
    ax.add_patch(p)

    #ax.text(xlim[0]+0.8*x_width, ylim[0]+0.05*ywidth[0], r"$|e| = %.2f $"%(scale_length), transform = ax.transAxes, horizontalalignment='left')
    legend_label = (r"$|e| = %.2f $"%(scale_length)).rstrip()
    ax.text(xlim[0], ylim[1]+0.1*y_width, legend_label,  horizontalalignment='left')

    if save_path is not None: pl.savefig(save_path, bbox_inches = "tight")

    return f, ax
