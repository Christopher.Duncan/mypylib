from . import general

    
def imageShow(z, ax = None, x = None, y = None, plotit = False, ext = None, **kwargs):
    """
    Plots an image (z) defined on grid given by x and y, where z is defined as z[x,y] (i.e. first dimension is row, which is x-specific).

    ax: Instance of matplotlib axis
    x,y: grid on which z is defined (default: None)
    z: image, defined as z[x,y]
    
    --kwargs:
    title
    xlabel
    ylabel
    interpolation
    cbar: If False, then axes are adjusted to allow vertical colorbar to the right of the axes

    Improvements:
    -- Edit to allow horizontal colorbar on top or bottom 
    -- Use rc param initialise
    """
    
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    
    if('interpolation' in kwargs):
        interp = kwargs['interpolation']
    else:
        interp = 'none'

    if('zlabel' in kwargs):
        zlab = kwargs['zlabel']
    else:
        zlab = ''
        
    ##Colors
    if('cbar' in kwargs):
        cbar = kwargs['cbar']
    else:
        cbar = True

    if('vmin' in kwargs):
        vmin = kwargs['vmin']
    else:
        vmin = None
    if('vmax' in kwargs):
        vmax = kwargs['vmax']
    else:
        vmax = None
        
        
    if('cmap' in kwargs):
        cmap = kwargs['cmap']
    else:
        cmap = 'rainbow'

    if('norm' in kwargs):
        norm = kwargs['norm']
    else:
        norm = None

    if('aspect' in kwargs):
        aspect = kwargs['aspect']
    else:
        aspect = 'auto'

    if(ext is None and x is not None and y is not None):
        if(z.shape[0] != x.shape[0]):
            raise RuntimeError("mypylib.plot.images.imageShow : image grid (1st dim) not conformal with x grid ")
        if(z.shape[1] != y.shape[0]):
            raise RuntimeError("mypylib.plot.images.imageShow : image grid (2nd dim) not conformal with y grid ")



        #Define extent
        dx = [x[1]-x[0], x[-1]-x[-2]]
        dy = [y[1]-y[0], y[-1]-y[-2]]
    
        ext = [x[0]-(dx[0]/2.), x[-1]+(dx[1]/2.), 
               y[0]-(dy[0]/2.), y[-1]+(dy[1]/2.)]

    if(ax is None):
        import pylab as pl; f = pl.figure(); ax = f.add_subplot(111)

    im = ax.imshow(z.T, extent = ext, interpolation = interp, origin = 'lower', norm = norm, cmap = cmap,
                   aspect = aspect, vmin = vmin, vmax = vmax)

    if(cbar):
        #Adjust for colorbar
        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size = "5%", pad = 0.1)

        plt.colorbar(im, cax = cax, orientation = 'vertical', label = zlab)

    general.set_axes_labels(ax,**kwargs)

    if(plotit):
        import pylab as pl
        pl.show()
    
    return ax
    
def imageDifference(a,b, axes = None, title = None, plotit = False, *imArgs, **imKwargs):
    """
    Plot on three (column-ordered) axes a, b, and then the difference

    :param a: Input image
    :param b: Input image
    :param axes: List (length 3) of axes to use
    :param title: List (len 2) of titles to assign to each plot, labelling image 1 and 2. The third title is autmatically assigned
    :param imArgs: Arguments for imageShow, shared across all axes
    :param imKwargs: Keyword arguments for imageShow, shared across all axes
    """

    if(axes is None):
        import pylab as pl
        f, axes = pl.subplots(ncols = 3)
    else:
        assert len(axes) == 3, "mypylib:plot:images:imageDifference: Axes entered should be alist of length three"

    if(title is None):
        tit = ["a","b","a-b"]
    else:
        assert len(title) == 2, "mypylib:plot:images:imageDifference: Titles entered should be alist of length two"
        tit = title
        tit.append(tit[0]+'-'+tit[1])
        
    imageShow(a,   ax = axes[0], title = tit[0], *imArgs, **imKwargs)
    imageShow(b,   ax = axes[1], title = tit[1], *imArgs, **imKwargs)
    imageShow(a-b, ax = axes[2], title = tit[2], *imArgs, **imKwargs)
    
    if(plotit):
        import pylab as pl
        pl.show()
    
