import pylab as pl
import numpy as np

def sample1D(sample, method = 'histstep', plotit = False, bins = 100, ax = None, **kwargs):
    """
    Plot on axis ax the distribution represented by sample.

    :param sample: 1D numpy array or list of sample points
    :param method: Method to construct distribution. Supported values are: hist
    :param plotit: If true, construct figure and show to screen
    :param bins: If method requires biining, this is the number of bins to use
    :param ax: Axes instance on which to plot
    :param **kwargs: Plotting keyword arguements such as title, xlabel etc
    """
    if isinstance(sample, list):
        sample = np.array(sample)

    if(len(sample.shape) != 1):
        raise RuntimeError("mypylib:plot:distributions:sample1D: sample numst be a one-dimensional array")

    if(plotit):
        f = pl.figure()
        ax = f.add_subplot(111)

    if(ax is None):
        raise ValueError("mypylib:plot:distributions:sample1D: axes must be defined")

    lab = None
    if('label' in kwargs):
        lab = kwargs['label']
        
    if method.lower() == 'hist':
        ax.hist(sample, bins, normed = True, label = lab)
    elif(method.lower() == 'histstep'):
        ax.hist(sample, bins, histtype='step', stacked=False, fill=False, normed = True, label = lab)
    else:
        raise ValueError("mypylib:plot:distributions:sample1D: method not recognised")


    #Formalise plot based on possible entries in kwargs
    if('title' in kwargs):
        tit = kwargs['title']
    else:
        tit = ''

    if('xlabel' in kwargs):
        xlab = kwargs['xlabel']
    else:
        xlab = '$x$'

    if('ylabel' in kwargs):
        ylab = kwargs['ylabel']
    else:
        ylab = '$p(x)$'


    ax.set_title(tit)
    ax.set_xlabel(xlab)
    ax.set_ylabel(ylab)

    if(plotit):
        pl.show()
