import numpy as np
from astropy import constants

def rotateEll(e1, e2, theta):
    """
    Rotate ellipticity components to a new coordinate system defined as a rotation by polar (counterclockwise) angle theta with respect to their current axis
    """
    
    e1R = e1*np.cos(2.*theta) + e2*np.sin(2.*theta)
    e2R = e2*np.cos(2.*theta) - e1*np.sin(2.*theta)

    return e1R, e2R

def rotateEllFrame(theta, cart = None, tang = None):
    """
    Conversion between cartesian and tangential/cross frames of reference for the ellipticity

    :param theta: Rotation angle, in radians defined as the polar angle (counter-clockwise) from the cartesian frame to the tangential frame
    :param cart: Cartesian coordiantes. If none, conversion from tang/cross to cartesian is assumed. [0] is x, [1] is y
    :param tang: [0] is tangential, [1] is cross. If none, cart->tang is assumed.
    """
    if(cart is not None):
        return rotateEll(cart[0], cart[1], theta)
    elif(tang is not None):
        # Rotate tangential/cross to cartesian. This should be equivalent to a rotation by -theta I think. Coded is just the inverse of the cartesian -> tangential relation
        s1 = tang[0]*np.cos(2.*theta) - tang[1]*np.sin(2.*theta)
        s2 = tang[0]*np.sin(2.*theta) + tang[1]*np.cos(2.*theta)

        return s1,s2
    else:
        raise ValueError("mypylib:astro:lensing: rotEllFrame: either cartesian or tangential frames of reference must be supplied")


def rotateEllFrameByCentroid(x, y, cen, cart = None, tang = None):
    """
    Conversion between cartesian and tangential/cross frames of reference for the ellipticity, using source position, and centroe of origin

    :param x: Cartesian positional co-ordinate
    :param y: Cartesian positional co-ordinate
    :param cen: Cartesian origin around which roation is performed
    :param cart: Cartesian coordiantes. If none, conversion from tang/cross to cartesian is assumed. [0] is x, [1] is y
    :param tang: [0] is tangential, [1] is cross. If none, cart->tang is assumed.
    """

    x = np.array(x)
    y = n.array(y)

    dCor = (np.vstack((x,y)).T - cen).T

    from mypylib.utils.numutils import polarAngle2D
    theta = polarAngle2D(dCor[0], dCor[1])
    
    return rotateEllFrame(theta, cart = cart, tang = tang)


def sigmaCritical(zlens, zs):
    """
    Return an array of sigmaCritical for a given zlens and range of zs
    """
    G = constants.G.to('Mpc*km2/(Msun*s2)').value
    c = constants.c.to('km/s').value

    from . import distances
    #Set distances using Ami's default distance code, to match results. In future, it may be better to use astropy.cosmo, and not that Amis code requires [Mpc/h]
    Distances = distances.Distance()
    Dl = Distances.Da(zlens) #Matching Ami's

    beta = np.array([Distances.Da(zlens, zsource)/Distances.Da(zsource) for zsource in zs])

    import math

    if(np.any(Dl <= 0)):
        print("DL is zero for : ", zlens)
    if(np.any(beta <= 0)):
        T = beta <= 0
        print("beta is zero for : ", zlens, zs[T])
        
    
    return c**2/(4.*math.pi*G*Dl*beta)


class sigmaCriticalLookup1D:
    """
    Routine for the lookup of a one-dimensional sigma critical value for a single lens redshift, and for a range of redshifts.
    Multiple lens redshifts can be used, but there is not interpolation between the lens redhsifts, only 1D interpolation on a single lens redshift
    """
    def __init__(self, zlens, zsRange, nZs = 1000):
        """
        :param zlens: List of lens redshift ranges
        :param zsRange: List of upper limits for the tabulated source redshift. For queried z > zs, the default extrapolation of np.interp is used. If a single value is entered, then the same upper range is used for all zlens
        """
        
        if(isinstance(zlens, float)):
           zlens = [zlens]

        if(isinstance(zsRange, (float, int))):
            zsRange = np.ones(len(zlens))*zsRange

        _tol = 1.e-8
        self._sigCTab = []
        self._zs = []
        for izl,zl in enumerate(zlens):
            self._zs.append(np.linspace(zl+_tol, zsRange[izl], nZs))
            self._sigCTab.append(sigmaCritical(zl,self._zs[-1]))

    def get(self, z, index = 0):
        return np.interp(z, self._zs[index], self._sigCTab[index], left = 1.e250)
    
