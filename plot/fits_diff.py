import optparse
import pyfits
import numpy as np

def load_fits(path):
        """
        Loads a FITS File.
        :param path: Path to where FITS file is stored.
        :return: FITS data, in np.array format.
        """
        hdulist = pyfits.open(path)
        data = hdulist[0].data
        hdulist.close()
        return data

def save_fits(data, path):
        """
        Save FITS data.
        :param data: FITS data to save, np.array format.
        :param path: Path to where data is to be saved.
        """
        hdu = pyfits.PrimaryHDU(data)
        hdulist = pyfits.HDUList([hdu])
        hdulist.writeto(path, clobber=True)

if __name__ == '__main__':

	# Configure the options command line parsing.
	argopts = optparse.OptionParser("usage: %prog [options] arg1")
	argopts.add_option("-i", "--input", dest="input_fits", default="", type="string", help="Specify path to two input fits separated by a comma (e.g. /home/image1.fits,/home/image2.fits")
	argopts.add_option("-o", "--output", dest="output_fits", default="None", type="string", help="Specify path  output path to save new fits image.")

	# Load command line paramaters.
	(options, args) = argopts.parse_args()
	input_fits = options.input_fits.split(',')
	output_fits = options.output_fits

	# Check if all inputs are okay.
	if len(input_fits) is not 2:
		print('Input images != 2')
		exit()
	if output_fits == "None":
		print('Output file not specified')
		exit()
	if not output_fits.endswith('.fits'):
		print('Output file has to be saved as a .fits')
		exit()

	# Load the two images
	image_first = load_fits(input_fits[0])
	image_second = load_fits(input_fits[1])


	# Compute the difference and save to specified folder.
	diff = image_first - image_second
	print('Sum of Diff = %f' % (np.sum(diff)))

	out_cube = np.float32(np.zeros((3,image_first.shape[0], image_first.shape[1])))
	out_cube[0, : , :] = diff
	out_cube[1, : , :] = image_first
	out_cube[2, : , :] = image_second

	save_fits(out_cube, output_fits)

	print('Image Saved.')
