import numpy as np
import math

def smail_redshift(z, zmed, alpha = 2, beta = 1.5):
    """
    Return the Smail et al redshift distribution.
    
    :param z: Redshift (or array or redshifts) to return the distribution for
    :param zmed: Median redshift of the survey
    :param alpha: Exponent of the z/z_0 factor
    :param beta: Exponent of the argument of the exponential [exp(-(z/z_0)**beta)]

    Note: Requires alpha > 0 and beta(beta-1) > 0 (alpha > 0 and beta > 1) for normalisation
    to be correct

    """
    x = 1.412*z/zmed
    norm = math.gamma(1+alpha/(beta*(beta-1)))/beta
    norm *= 1.412/zmed/1.7832005773904782
    return norm*(x**alpha)*np.exp(-x**beta)
