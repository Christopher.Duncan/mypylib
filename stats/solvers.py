"""
This module contains code to sample and optimise functions
"""
import os
import warnings
import numpy as np

_modulelabel = __name__+"stats:solvers:"

def run_emcee(lnLFn, args, initial,
              nburn, nchain, niter,
              perturb, nwalkers = None,
              outputfiles_base = None,
              dump_interim = True,
              restart = False):
    """
    Run EMCEE on the likelihood function defined by lnLFn.
    :param lnLFn: Function with call signature lnLFn(params, *args), where params is a list/ndarray of length
        ndim
    :param initial: ndarray/list of initial parameter values
    :param args: Arguements (other than param) required by lnL value. Should not be updated at runtime.
    :param nburn: Number of burnin steps to take, per-walker.
    :param nchain: Number of chain steps to take, per-walker.
    :param niter: Number of steps to complete between intermediate output, per-walker.
    :param perturb: Width of Normal used to perturb the initial walkers states around initial.
        init_walker = N(mu = initial, sigma = perturb)
        If scalar, then this is applied to all parameters
        If list / ndim, then an new sigma is applied to each parameter
    :param nwalkers: Number of walkers. Must be > 2*ndim. If None-Type (default), then set to 2*ndim
    :param outputfiles_base: Basename used for file output. If None-Type, then no product is output. These files are
        output:
            -- chain.{R}.h5
            -- burn.{R}.h5  - only if nburn > 0
            -- iterim files if dump_interim is True
        where {R} is an integer which labels the run (starting from 0), and which is incremented if restart is used.
        This is inferred by finding a chain.{R-1}.h5 file. If a previous run only used burnin, but did not output
        a chain, then the new run will overwrite the old burnin file, otherwise new burnin and chain files will be
        produced.
    :param dump_interim: If True, then it will dump files containing current position, rng state and probs, which
        can be used to restart the MCMC (using restart = True).
    :param restart: If True, then the code will search for interim files using outputfiles_base. Only position needs
        to be found.
        -- If position is found, new chain is seeded off this position (note that the code will still burn
        in from this position if it is told to). If position is not found, a completely new chain is set up using
        perturb value.
        -- If rng state is found, the numpy rng is seeded using that state (via emcee). If not found, the default
            seed is used. In this case one should be careful in iterpreting the chain, as the disconnect may affect
            the validity of the chain.
        -- If probs are found, the chain is started using these probs (saves recalculating them). If not, then these
            are recalculated (see emcee docs).
        NB: It is up to the user to ensure the experimental set-up on the restart is the same as the original.
        NNB: The returned sampler only contains the current chain (not the old chains), and the current chain is output
            with it's own run identifier in the filename (see outputfiles_base docstring).

    :return:
    """
    import emcee
    from mypylib.utils.io import save_dict_to_hdf5

    if outputfiles_base is None: warnings.warn("SHE_fit_toolkit: model:solvers:run_emcee: Output basename not "
                                               "specified. No files will be output")

    do_init = True
    rstate0 = None; lnprob0 = None

    # Initial can be:
    # 1D array of list: npar long, best guess at state
    # 2D array or list: (npar, nwalkers), initial state for all walkers
    # First, ensure ndarray
    initial = np.array(initial)

    ndim = len(initial)

    if nwalkers is None:
        if initial.ndim == 2:
            nwalkers = initial.shape[1]
        else:
            nwalkers = 2*ndim

    # If initial is 2D, then we transpose it to get p0 as p0 = (nwalkers, ndim)
    if initial.ndim == 2:
        # First, make sure consistency with nwalkers
        assert nwalkers == initial.shape[1], "Internal inconsistency with entered nwalker and initial guess (2d)"
        p0 = initial.T
        # Then we do not need to init
        do_init = False

    # Detail form of sampling to user
    #logger.info("\n Running EMCEE with following setup: ")
    #logger.info("# walkers: %d" % nwalkers)
    #logger.info("# burn points: %d " % nburn)
    #logger.info("# chain points: %d" % nchain)
    #logger.info("# of iterations per update: %d " % niter)
    #logger.info("Initial perturbation: " + str(perturb))
    #logger.info("Initial Parameters : "+str(initial))
    #logger.info("\n")

    runExt = ".0"

    print("Will restart?", restart)
    if restart:
        print("Restarting emcee...")
        def load_interim():
            from pickle import load
            if outputfiles_base is None: return None

            init = [None,None,None]
            # Read in position dump
            try:
                init[0] = np.genfromtxt(outputfiles_base+"interim_position.txt")
            except:
                warnings.warn("Failed to find interim position at %s"%(outputfiles_base+"interim_position.txt"))
                return None

            # Read in probability dump
            try:
                init[1] = np.genfromtxt(outputfiles_base+"interim_prob.txt")
            except:
                init[1] = None

            # Read in state
            try:
                with open(outputfiles_base+"interim_state.obj", 'rb') as f:
                    init[2] = load(f)
            except:
                init[2] = None

            return init

        init_state = load_interim()
        if init_state is None:
            # Failed to restart
            do_init = True
            warnings.warn(_modulelabel+"run_emcee: Restart requested, but failed to successfully restart"
                                       "MCMC. Re-initialising.")
        else:
            # Will restart
            do_init = False
            p0 = init_state[0].reshape((nwalkers, ndim))

            #logger.info(".... Restarting EMCEE at position: "+str(p0))
            # print(".... Restarting EMCEE at position: "+str(p0))

            # Find and iterate the run code
            run_iter = 0
            while os.path.isfile(outputfiles_base+"chain."+str(run_iter)+".h5"):
                run_iter+=1
            runExt = "."+str(run_iter)

            lnprob0 = init_state[1]
            if lnprob0 is None: warnings.warn(_modulelabel+" run_emcee: Restart successful but could not "
                                                           "find interim_prob. Assuming None")

            rstate0 = init_state[2]
            if rstate0 is None: warnings.warn(_modulelabel + " run_emcee: Restart successful but could not find "
                                                             "interim_prob. Assuming None")

    # Set up default state for all walkers
    # Perturb the input samplers by a certain amount
    if do_init:
        # If we reach this point, then initial is 1D (over par) and we have not restarted
        rstate0 = None; lnprob0 = None

        # Make perturb a 1D array of size npar if not list / array
        if isinstance(perturb, list): perturb = np.array(perturb)
        if isinstance(perturb, (float, np.float, int, np.int)):
            perturb = perturb*np.ones(ndim)

        assert len(perturb) == ndim, "EMCEE init, perturb must be ndim length array"

        # (nwalkers, ndim)
        rand = np.random.normal(loc = 0, scale = perturb, size = (nwalkers, ndim))

        # As sample from N(initial, perturb)
        # Shift mean
        p0 = initial.reshape((1,-1)) + rand

        # As pc change
        '''
        p0 = np.zeros((nwalkers, ndim))
        for i in range(initial.shape[0]):
            rand = np.random.normal(loc = 0., scale=perturb, size=nwalkers)
            if np.isclose(initial[i], -1.):
                # Special case as for parameter = -1, the randomness of the general case is cancelled out
                p0[:,i] = initial[i] + rand
            else:
                # This treats perturb as a pc change
                p0[:, i] = rand+ (initial[i]*(1.+rand))

            #print("For parameter %d, initial state is %s"%(i, p0[:,i]), rand)
        '''

    from pickle import dump
    def _interim_output(sampler, base, file_ext, state = None):
        if base is None: return

        filename = base+file_ext+".h5"

        oDict = {"flatchain":sampler.flatchain,
                 "pchain":sampler.flatlnprobability}
        save_dict_to_hdf5(dic=oDict,filename=filename)

        if state is not None and dump_interim:

            # Dump the interim state
            with open(base+"interim_state.obj", "wb") as f:
                dump(state["state"], f)

            np.savetxt(base+"interim_position.txt", state["p0"])
            np.savetxt(base+"interim_prob.txt", state["prob"])

    # Define the sampler
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnLFn, args=args)

    # Do the burn-in
    if nburn > 0:
        #logger.info("Burning in...")
        iteration = 0

        while iteration < nburn:
            # Note: recent edit to emcee required lnprob0 -> log_prob0
            p0, lnprob0, rstate0 = sampler.run_mcmc(p0, niter, log_prob0=lnprob0, rstate0=rstate0)
            # ndarray(nwalker, ndim), ndarray(nwalker), tuple(str, ndarray, int, int, float)

            _interim_output(sampler, outputfiles_base, "burn"+runExt,
                            {"p0":p0, "prob":lnprob0, "state":rstate0})

            iteration+=niter

        sampler.reset()
        #logger.info("... Finished burn-in")

    # Run the chain
    iteration = 0
    #logger.info("Running MCMC (emcee)...")
    while iteration < nchain:
        p0, lnprob0, rstate0 = sampler.run_mcmc(p0, niter, log_prob0=lnprob0, rstate0=rstate0)

        _interim_output(sampler, outputfiles_base,"chain"+runExt,
                        {"p0": p0, "prob": lnprob0, "state": rstate0})

        iteration+=niter
        #logger.debug("Finished iteration {0:d} of {1:d} ".format(iteration, nchain))

    #logger.info("...Finished MCMC (emcee)")

    # print("Finished at ", p0)

    return sampler

def _multinest_default_args():
    """
    Default runtime args for MultiNest
    :return:
    """
    return dict(n_params=None,
                   n_clustering_params=None, wrapped_params=None,
                   importance_nested_sampling=False,
                   # NOTE: If this is true, then multimodal is switched off - cannot do both
                   multimodal=True, const_efficiency_mode=False, n_live_points=400,
                   evidence_tolerance=0.5, sampling_efficiency=0.8,
                   n_iter_before_update=100, null_log_evidence=-1e90,
                   max_modes=100, mode_tolerance=-1e90,
                   outputfiles_basename="chains/1-", seed=-1, verbose=True,
                   resume=False, context=0, write_output=True, log_zero=-1e100,
                   max_iter=0, init_MPI=False, dump_callback=None)

def run_multinest(lnLFn, priorfn = None, n_dim = None, prior_support_low = None, prior_support_high = None, **nestKwArgs):
    """

    :param lnLFn: Function to calculate log-likelihood
    :param prior_support_low: ndarray of shape (nDim,) giving lower boundary of support for each parameter.
        Note that pymultinest only currently supports a uniform prior.
    :param prior_support_high:
    :param nestKwArgs: Multinest keyword arguments not represented in general call. See _multinest_default_args.
    :return:
    """

    import pymultinest

    def uniform_prior(cube):
        """
        Unit-cube to physical parameter-space conversion according to conservation of prior mass, required by MultiNest
        """
        return cube * (prior_support_high - prior_support_low) + prior_support_low

    if priorfn is None:
        assert prior_support_high is not None and prior_support_low is not None, "Prior must be given"
        priorfn = uniform_prior
        n_dim = len(prior_support_low)
        assert len(prior_support_high) == n_dim, "solvers: run_multinest: Prior support arrays are not conformal"
    else:
        assert n_dim is not None, "ndim must be specified is priorfn passed"

    # Default run-time arguments
    runArgs = _multinest_default_args()
    runArgs["n_clustering_params"] = n_dim
    runArgs["n_params"] = n_dim

    runArgs.update(nestKwArgs)

    #if runArgs["init_MPI"]: warnings.warn(__name__+":run_multinest: "
    #                                      "multinest is set to use MPI. This is likely to conflict with any MPI-based"
    #                                      "set-up currently used as part of the fit toolkit, and is not recommended. "
    #                                      "I will continue, but be aware. ")

    # Make directory
    print("Directory: ", os.path.isdir(os.path.dirname(runArgs["outputfiles_basename"])))
    if not os.path.isdir(os.path.dirname(runArgs["outputfiles_basename"])):
        print("Making directory!")
        os.makedirs(runArgs["outputfiles_basename"])

    runArgs.update(dict(LogLikelihood = lnLFn, Prior = priorfn, n_dims = n_dim,))
    result = pymultinest.solve(**runArgs)

    sample = result['samples']
    print("Sample shape: ", sample.shape)
    print("Sample Mean, StD: ", sample.mean(axis = 0), sample.std(axis = 0))

    # Add in file creation to show finished
    try:
        # May be python 3.4+ - not sure how this fits into Euclid spec
        from pathlib import Path
        Path(os.path.join(os.path.dirname(runArgs["outputfiles_base"]),"finished")).touch()
    except:
        # Do nothing.
        _ = 0

    return result