import numpy as np

def dChi2_CI(CI, dof):
    """
    Return delta Chi2 which gives the CI confidence interval (for a gaussian), by minimising Gammainc(dof/2, delta_chi)-CI (see NR 15.6)
    """
    
    from scipy.special import gammainc
    import scipy.optimize as opt

    assert (CI>0.) and (CI < 1.), "mypylib:dChi2_CI, Confidence interval must be less than 1 and greater than zero. Given %f"%(CI)
    
    def cost(x):
        if(x < 0.):
            return 1.e300
        return abs(gammainc(dof/2.,x/2.)-CI)
    
    #Find minimum
    mini = opt.fmin(cost, x0 = 5., disp = 0)[0]
    
    return mini

def lnL_interpolate_KDE(sample, interpPoints, mlPoint = None, mlOptimiser = None):
    """
    Return the log_likelihood interpolator for a given sample of parameters

    :param sample: numpy array of dimension [nSample, nDim]
    :param interpPoints: numpy array [nSample, nDim] on which to interpolate the log-likelihood values
    :param mlPoint: The maximum-likelihood point. If not none, the returned points ate dlnL = lnL_max-lnL_interp
    """
    
    #Define the KDE interpolator
    from scipy.stats import gaussian_kde
    kde = gaussian_kde(sample.T)

    #Evaluate the KDE
    inter = kde(interpPoints.T) #.T?

    if(mlPoints is not None):
        lnLMax = kde(mlPoint)

        inter = lnLMax - inter
    elif(mlOptimiser is not None):
        raise ValueError("This hasn't been found yet")

    return inter

def contour_area_from_sample(sample_in, lnL = None, CI = 0.683, plot = False, asLength = True, margIndex = None):
    """
    Find a value for the volume contained based on confidence interval CI. This iteration assumes Gaussian posteriors
    (used for setting contour thresholding)

    NOTE: For a sample, the lnL entered must correspond to the whole sample to be accurate: i.e. all dimensions on which
    the sample was take must be present. If a sample is taken out (e.g. by marginalisation), then the results may not
    make sense

    :param sample: numpy array of dimensions [nSample, nDim] (used for contour thresholding)
    :param lnL: nSample list of lnL values for each point in parameter space. If none, this is estimated by KDE smoothing
    :param CI: Percentage confidence interval to take as boundary
    :param asLength: If true, return the volume enclosed rescaled to a length (L = V^{1/dof})
    :param plot: Produce debug plots. These must be invoked with pl.show() after calling
    """

    #If marg index is not none, then KDE smooth and interpolate the lnL
    # Identify subsample to consider
    if(margIndex is not None):
        print("Marginalising")
        tSample = np.ones(sample_in.shape[0]).astype(np.int)
        tSample[margIndex] = 1
        tSample[-1] = 0 #Remove lnL
        sample = sample_in[tSample,:]

    #Deconstruct sample
    sample = sample_in[:,:]
    dof = sample.shape[1]    
    
    #Get contour level by compariosn with gaussian *NOTE: THis means strong non-Gaussianity may affect reliability of results*
    dChi = dChi2_CI(CI, dof)
    
    #Monte-Carlo integrate to get area corresponding to that CI
    #Construct the samples on which to interpolate
    nMC = 100000

    from scipy.stats import uniform
    uni = uniform.rvs(size = (nMC, dof)) 

    support_min = np.min(sample, axis = 0)
    support_max = np.max(sample, axis = 0)
    
    #Convert from uniform to support scales in each direction
    rand = np.zeros(uni.shape)
    delta = support_max-support_min
    rand = uni*np.tile(delta, (nMC,1)) + np.tile(support_min, (nMC, 1)) #(nMC, dof)
    
    # #KDE
    # from scipy.stats import gaussian_kde
    # kde = gaussian_kde(sample.T)
    # #Evaluate the KDE
    # inter = kde(rand.T) #.T?
    # #Convert to log_PDF, and then deltaChi - How, unless we know the scaling?


    #Get lnL
    if(lnL is None):
        print("--Applying KDE interpolation for lnL determination")
        lnL = lnL_interpolate_KDE(sample, sample)

    #Get lnL values for all these parameter values
    dlnL = lnL-np.max(lnL)    

    #This is possibly wasted effort if KDE interpolation is used
    from scipy.interpolate import griddata
    inter = griddata(sample, dlnL, rand, method = "nearest")
    
    #Renormalise by lnL max, and multiply by 2 to get dChi2
    inter =  -2.*(inter)
    
    #Count the number of successes
    area = (inter < dChi).sum()

    
    area /= float(nMC)
    
    area *= np.prod(delta)
    
    if(asLength):
        area = np.power(area,1./dof)
    
    if(plot):
        success = rand[inter<dChi] #Shape (nSuccess, dof)

        # import corner
        # corner.corner(sample[...,:-1])
        
        import mypylib.plot.distributions as mydist
        import pylab as pl
        ax = pl.subplot()
        #mydist.sample1D(sample[:,0], "histstep", ax = ax)
        ax.axvline(np.nanmin(success[:]), ls = '--')
        ax.axvline(np.nanmax(success[:]), ls = '--')
        mydist.sample1D(success[:], "histstep", ax = ax)
        #ax.plot(testG, testInter)
        #mydist.sample1D(rand[:,0], "histstep", ax = ax)
        
    # import pylab as pl
    # ax = pl.subplot()
    # x = np.linspace(support_min, support_max, 1000)
    # ax.plot(x, griddata(sample, sample[:,-1], x))
    # pl.show()
    
    # import pylab as pl
    # ax = pl.subplot()
    # #ax.hist(success, bins = 100)
    # ax.scatter(success[:,0], success[:,1], marker = "x")
    # pl.show()

    
    return area

def figure_of_merit(*args, **kwargs):
    """
    Overloaded version of contour_area_from_sample
    """
    
    return 1./contour_area_from_sample(*args,  **kwargs)

def inverseSampleFromDist(x,p, size = 1, allowRenorm = False, tol = 1.e-6):
    """
    Sample from the distribution p, evaluated on grid x, via the CDF
    """
    _debug = False
    _tol = tol
    
    from scipy import random
    
    #Generate the random uniform samples required
    _urand = random.uniform(size = size)

    if(_debug):
        import mypylib.plot.distributions as pltdist
        pltdist.sample1D(_urand, plotit = True)
    
    import numpy as np
    ##Get cumulative distribution
    dx = np.diff(x); dx = np.hstack((dx[0], dx))
    cum = np.cumsum(p)*dx

    if allowRenorm:
        cum /= cum[-1]

    if(abs(cum[-1]-1) > _tol):
        print("Integrated dist:", np.trapz(p,x))
        print("Cumulative max:", cum[-1])
        raise ValueError("mypylib:stats:sampling:inverseSampleFromDist: Cumulative sum not close to one")
    
    if(_debug):
        import pylab as pl
        f = pl.figure()
        ax = f.add_subplot(111)
        ax.plot(x[:cum.shape[0]], cum)
        pl.show()


    sample = np.interp(_urand, cum, x)

    return sample
