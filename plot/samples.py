"""
This contains the code for using plots using statistical samples

Author: cajd
"""
import pylab as pl
import numpy as np

def walk1D(sample, plotit = False, ax = None, legend = True):
    """
    Plot a one dimensional walk for a sample

    :param sample: The sample over parameter space. Numpy array, of construction (nSamples, nWalks), where nSamples is the number of steps taken, and nWalks are the number of independent walks (each plotted separately).
    :param plotit: If true, the plot is output to screen
    :param ax: Axes on which to construct plot. If plotit is false, this must be defined
    :param legend: (Default True). If True, add a legend (loc = 0) to the axis.
    """

    if(plotit):
        f = pl.figure()
        ax = f.add_subplot(111)

    if(ax is None):
        raise ValueError("mypylib:plot:distributions:sample1D: axes must be defined")

    steps = np.arange(sample.shape[0])

    if(len(sample.shape) > 2):
        raise ValueError("mypylib:plot:samples:walk1D: sample must be either 1D or 2D, not more")
    
    if(len(sample.shape) == 1):
        sample = sample.reshape((sample.shape[0], 1))
        
    for i in range(sample.shape[1]):
        ax.plot(steps, sample[:,i], label = str(i))

    if(legend):
        ax.legend(loc = 0)
    
    if(plotit):
        pl.show()
    

##Overloaded routines
def sample1D(**args):
    """
    Overloaded version of distributions:sample1D
    """
    from . import distributions
    return distributions.sample1D(args)
