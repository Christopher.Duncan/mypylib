"""

This module contains helper routines at os path level

Author: cajd

"""
import os


def recursive_mkdir(path):
    assert isinstance(path, str), "mypylib:myos:mypath:recursive_mkdir: Path entered must be a string."

    splitPath = path.split("/")

    if(path[0] != '/'):
        #Take from the cwd
        splitPath  = [os.getcwd()]+splitPath
    else:
        #Take from the root
        splitPath[0] = '/'

    for i in range(2,len(splitPath)+1):
        tPath = os.path.join(*(splitPath[:i]))

        print("Creating path at ", tPath)

        if(not os.path.isdir(tPath)):
            os.mkdir(tPath)
