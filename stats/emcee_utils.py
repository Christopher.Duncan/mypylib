"""
Utility routines for emcee type output
"""

def cut_burnin(chain, pchain, nwalkers, ncut):
    """

    :param chain:
    :param pchain:
    :param nwalkers:
    :param ncut:
    :return:
    """
    import numpy as np

    nlink = chain.shape[0]

    nperwalker = nlink//nwalkers

    todelete = []
    for W in range(nwalkers):
        todelete.append(np.arange(W*nperwalker, W*nperwalker+ncut))

    todelete = np.hstack(todelete)

    return np.delete(chain,todelete,axis = 0), np.delete(pchain, todelete, axis = 0)