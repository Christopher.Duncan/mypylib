"""
Utility functions for numpy
"""
import numpy as np


def get_data_base(arr):
    """For a given NumPy array, find the base array
    that owns the actual data."""
    base = arr
    while isinstance(base.base, np.ndarray):
        base = base.base
    return base


def arrays_share_data(x, y):
    """
    Test whether two arrays share the same underlying data
    :param x:
    :param y:
    :return:
    """
    return get_data_base(x) is get_data_base(y)