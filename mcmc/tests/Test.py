import numpy as np
import scipy.integrate as integrate
import likelihoods
from scipy import random
import emcee

##Initialise to recognise the mcmc routines
import sys
import os
sys.path.insert(0, os.path.abspath("../"))
import methastings

##MCMC declarations
nBurn = 1000
nStep = 10000
nChain = 6

propWidth = 20.

#Set the testing to run
testID = 4

#Set log likelihood to use
logL = likelihoods.logL_sumGauss_2d
nDim = 2

def mh_test1_singleChain():
    prop = methastings.gaussianProposal(propWidth)

    mcmc = methastings.mhmcmc(prop, logL, np.array([8.5]))
    
    mcmc.nSteps(numStep = nStep, nBurn = nBurn)

    chain = mcmc.getChain()

    print("Mean: ", chain.mean(axis = 1))
    print("StD: ", chain.std(axis = 1))
    print("Acceptance Rate: ", mcmc.acceptanceRate())

    
    return chain, mcmc


def mh_test2_multipleChain():
    prop = methastings.gaussianProposal(propWidth)

    initSupport = np.array([4., 20.]).reshape((1,2))
    
    mcmcmc = methastings.mchain_mhmcmc(nChain, 1000, initSupport = initSupport, proposal = prop, logL = logL)

    mcmcmc.run(nBurn = nBurn,maxStep = 10000)

    chain = mcmcmc.getChain(collected = True)
    
    print("Mean: ", chain.mean(axis = 1))
    print("StD: ", chain.std(axis = 1))
    print("Acceptance Rate: ", mcmcmc.acceptanceRate())
    
    return chain, mcmcmc


def emcee_test1():
    print("Testing EMCEE")
    
    nWalkers = 50
    nBurn = 50
    nStep = 300

    #Set initial guess for each walker
    p0 = random.uniform(0, 20, size = nDim*nWalkers).reshape((nWalkers,nDim))
    
    #Burnin
    sampler = emcee.EnsembleSampler(nWalkers, nDim, logL) #If logL requires args, this can be passed is  as args = [] in order

    #Burn in
    pos, lnProb, state = sampler.run_mcmc(p0, nBurn)

    sampler.reset()

    print("--Finished Burnin")
    
    #Run the main run
    sampler.run_mcmc(pos, nStep)

    lnLikelihoodEval = sampler.flatlnprobability
    chain = sampler.flatchain

    lnLikelihoodEval = lnLikelihoodEval.reshape((lnLikelihoodEval.shape[0], 1))
    
    np.savetxt("chains/EMCEE_chain.txt", chain)
    print("Output to : ", "chains/EMCEE_chain.txt")

    #Append log-likelihood onto end of chain (as if it's an extra parameter)
    stack = np.hstack((chain, lnLikelihoodEval))

    np.savetxt("chains/EMCEE_stack.txt", stack)
    print("Output to : ", "chains/EMCEE_stack.txt")

    print("\n ML point at: (Params,.., lnL) ", stack[np.argmax(stack[:,2]),:])
    
    #Plot using corner
    import corner
    corner.corner(chain)
    import pylab as pl
    pl.show()
    
    ##flatchain is of dim (nwalkers*nstep, nPar)
    return sampler.flatchain.T, sampler

def multinest_test1():
    print("Testing MultiNest")

    #NOTE: To find documentation to the acceptable arguments for the pymultinest solver, look at pymultinest.run.run docstring. Everything here is pass-able, and on top of this, the solve routine can take:
    #(Defined in pymultinest.solve docstring
    #Prior: Mapping betwen unit cube and prior support range
    #LogLikelihood: As described, in physical space
    #nparams: Dimensionality of the problem

    #See http://johannesbuchner.github.io/PyMultiNest/pymultinest_run.html
    
    import pymultinest
    
    uni_prior_low = np.array([4.]*nDim)
    uni_prior_high = np.array([20.]*nDim)
    def uniform_prior(cube):
        """
        Unit-cube to physical parameter-space conversion according to conservation of prior mass, required by MultiNest
        """
        return cube*(uni_prior_high-uni_prior_low) + uni_prior_low
    
    #Replicate runArgs here for completeness, paedagogical reasons
    runArgs = dict(n_params = None, 
                   n_clustering_params = nDim, wrapped_params = None, 
                   importance_nested_sampling = False, #NOTE: If this is true, then multimodal is switched off - cannot do both
                   multimodal = True, const_efficiency_mode = False, n_live_points = 400,
                   evidence_tolerance = 0.5, sampling_efficiency = 0.8, 
                   n_iter_before_update = 100, null_log_evidence = -1e90,
                   max_modes = 100, mode_tolerance = -1e90,
                   outputfiles_basename = "chains/1-", seed = -1, verbose = True,
                   resume = False, context = 0, write_output = True, log_zero = -1e100, 
                   max_iter = 0, init_MPI = False, dump_callback = None)

    result = pymultinest.solve(LogLikelihood = logL, Prior = uniform_prior, n_dims = nDim, **runArgs)

    sample = result['samples'].T
    print("Sample shape: ", sample.shape)
    print("Sample Mean, StD: ", sample.mean(axis = 1), sample.std(axis = 1))

    #Use analyser to read in output - See Analyzer docstring for more information on output
    analyze = pymultinest.analyse.Analyzer(1, runArgs['outputfiles_basename'])
    print("Analyser Stats output: ")
    print(analyze.get_mode_stats())

    print("Maximum Likelihood point at: ", analyze.get_best_fit())
    
    print("Plotting with pymultinest plot routines:")
    pltMarg = pymultinest.plot.PlotMarginalModes(analyze)
    pltMarg.plot_modes_marginal(0)
    print(".. Done that. Will be shown with next pl.show")

    import corner
    corner.corner(analyze.get_equal_weighted_posterior()[:,:-1])

    import pylab as pl
    pl.show()
    
    return sample, result

if __name__ == "__main__":

    # ##Probe using linear space
    # x = np.linspace(4., 20., 5000)
    # print x.shape
    # lP = logL(x)
    # P = np.exp(lP - np.max(lP))
    
    # #Renormalise for plotting
    # P /= integrate.trapz(P,x)

    if(testID == 1):
        chain, mcmc = mh_test1_singleChain()
    elif(testID == 2):
        chain, mcmc = mh_test2_multipleChain()
    elif(testID== 3):
        chain, mcmc = emcee_test1()
    elif(testID == 4):
        chain, disc = multinest_test1()
    else:
        raise ValueError("testID not valid")
        
    # import pylab as pl
    # f = pl.figure()
    # ax = f.add_subplot(111)
    
    # ax.hist(chain[0,:], bins = 100, normed = True, label = 'Sampled')
    # # ax.plot(x,P, label = 'True')
    
    # ax.legend(loc = 0)
    # ax.set_xlabel(r'$x$', fontsize = 16)
    # ax.set_ylabel(r'$P(x)$', fontsize = 16)
    
    # pl.show()
