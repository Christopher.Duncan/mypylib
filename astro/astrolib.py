from math import pi

"""
Author: Ami Choi.
"""

# TODO: create trigonometric function definitions like
#	    cos = lambda x : math.cos(x*math.pi/180.)
#	 and get rid of all of the DEGRAD stuff.


# Conversion from degrees to radians
DEGRAD = pi/180.
# Speed of light
c = 299792458.

# Magnitude of sun in different filters (dunno if this is AB or Vega.....)
msun = {}
msun['U'] = 5.62
msun['B'] = 5.48
msun['V'] = 4.83
msun['R'] = 4.47
msun['I'] = 4.11
msun['u'] = 6.55
msun['g'] = 5.12
msun['r'] = 4.68
msun['i'] = 4.57
msun['z'] = 4.54

# SDSS mstar values from Zandivarez et al.  Conversion to Johnson/Cousins
#   based on Robert Lupton's SDSS transformations.
mstar = {}
mstar['u'] = -17.83
mstar['g'] = -19.71
mstar['r'] = -20.70
mstar['i'] = -21.08
mstar['z'] = -21.47
mstar['B'] = mstar['u'] - 0.8116*(mstar['u']-mstar['g']) + 0.1313
mstar['V'] = mstar['g'] - 0.5784*(mstar['g']-mstar['r']) - 0.0038
mstar['R'] = mstar['r'] - 0.2936*(mstar['r']-mstar['i']) - 0.1439
mstar['I'] = mstar['i'] - 0.3780*(mstar['i']-mstar['z']) - 0.3974



"""
All distance measurements assume k=0. The return values are h_inv Mpc. The
  integration appears to be accurate to about 10^9 (ie about the same
  accuracy as the speed of light used.
"""
#
# All Distance Units are h**-1 Mpc
#
# Comoving distance
def comoving_distance(z1,z2=0.,omega_m=0.27,omega_l=0.73):
	from scipy import integrate
	if z2<z1:
		z1,z2 = z2,z1
	f = lambda z,m,l : (m*(1.+z)**3+l)**-0.5
	return c*integrate.romberg(f,z1,z2,(omega_m,omega_l))/1e5

# Angular diameter distance between redshifts z1 and z2
def angular_diameter_distance(z1,z2=0.,om=0.27,ol=0.73):
	if z2<z1:
		z1,z2 = z2,z1
	return comoving_distance(z1,z2,om,ol)/(1.+z2)

# Luminosity distance to a redshift
def luminosity_distance(z,omega_m=0.27,omega_l=0.73):
	return (1.+z)*comoving_distance(z,omega_m=omega_m,omega_l=omega_l)


"""
Some celestial units tools....
"""
# Check if the coordinate is in degrees...really just makes sure it's a float.
def is_degree(comp):
        if type(comp)==float:
                return True
        return False

# Convert string ra to degrees
def ra2deg(ra):
        comp = ra.split(" ")
        if comp[0]==ra:
                comp = comp[0].split(":")
        deg = float(comp[0])+float(comp[1])/60.+float(comp[2])/3600.
        return deg*15.

# Convert string declination to degrees
def dec2deg(dec):
        comp = dec.split(" ")
        if comp[0]==dec:
                comp = comp[0].split(":")
        if comp[0][0]=="-":
                comp[0] = comp[0][1:]
                sign = -1.
        else:
                sign = 1.
        return sign*(float(comp[0])+float(comp[1])/60.+float(comp[2])/3600.)

# Convert decimal ra to HMS format
def deg2ra(ra,sep=" "):
        ra /= 15.
        h = math.floor(ra)
        res = (ra-h)*60.
        m = math.floor(res)
        s = (res-m)*60.
        if sep=="hms":
                sep1 = "h"
                sep2 = "m"
                sep3 = "s"
        else:
                sep1 = sep
                sep2 = sep
                sep3 = ""
        return "%02d%s%02d%s%06.3f%s" % (h,sep1,m,sep2,s,sep3)

# Convert decimal declination to DaMaS format
def deg2dec(dec,sep=" ",addsign=False):
        if dec<0:
                sign = -1.
                dec = abs(dec)
        else:
                sign = 1.
        d = math.floor(dec)
        res = (dec-d)*60.
        m = math.floor(res)
        s = (res-m)*60.
        if sep=="dms":
                sep1 = "d"
                sep2 = "m"
                sep3 = "s"
        else:
                sep1 = sep
                sep2 = sep
                sep3 = ""
        if sign==-1:
                return "-%02d%s%02d%s%06.3f%s" % (d,sep1,m,sep2,s,sep3)
        if addsign:
                return "+%02d%s%02d%s%06.3f%s" % (d,sep1,m,sep2,s,sep3)
        return "%02d%s%02d%s%06.3f%s" % (d,sep1,m,sep2,s,sep3)

"""
Determine the angular distance in arcsecs between two celestial points.
  Distance is returned as arcsecs, and method chooses a more or less accurate
  way of determining the distance (with 1 being the fastest/least accurate).
"""
# UNITS: arcsec (output), degrees (input)
def angular_distance(ra1,dec1,ra2,dec2,method=1):
	import scipy
	if scipy.isscalar(ra1) and scipy.isscalar(ra2):
		from math import cos,sin,sqrt
		if ra1-ra2>180:
			ra1 -= 360.
		elif ra2-ra1>180:
			ra2 -= 360.
	else:
		from scipy import cos,sin,sqrt
		if scipy.isscalar(ra1):
			t = ra2.copy()
			ra2 = ra2*0. + ra1
			ra1 = t.copy()
			t = dec2.copy()
			dec2 = dec2*0. + dec1
			dec1 = t.copy()
			del t
		
		ra1 = scipy.where(ra1-ra2>180,ra1-360.,ra1)
		ra2 = scipy.where(ra2-ra1>180,ra2-360.,ra2)

	ra1 = ra1*DEGRAD
	dec1 = dec1*DEGRAD
	ra2 = ra2*DEGRAD
	dec2 = dec2*DEGRAD

	if method==1:
		deltadec = dec1-dec2
		deltara = (ra1-ra2)*cos(dec2)
	else:
		div = 1.
		if method==3:
			div = sin(dec2)*sin(dec1)+cos(dec2)*cos(dec1)*cos((ra1-ra2))
		deltara = cos(dec2)*sin(ra1-ra2)/div
		deltadec = sin(dec2)*cos(dec1)-cos(dec2)*sin(dec1)*cos(ra1-ra2)/div
	return sqrt(deltadec*deltadec+deltara*deltara)*3600./DEGRAD


"""
Optical and radio flux conversion routines. Note that I'm not sure if the
  magnitudes need to be AB or Vega; AB for the SDSS filters, I would guess.
"""
# Magnitude to luminosity.
#   UNITS: h**-2 L_sun * 10^12
def mag2lum(mag,z,band,omega_m=0.27,omega_l=0.73):
	distance = luminosity_distance(z,omega_m,omega_l)
	return distance**2 * 10**((msun[band]-mag)/2.5)/100.

# Radio flux to luminosity for a spectra index and redshift. From Peacock,
#   page 441, eqn 14.45. The 105.026 term is a conversion from Jy to Watts
#   (including converting from Mpc).
#   UNITS: 10^21 h**-1 Watts (output), Jy (input)
def flux2lum(flux,z,alpha=0.7):
	lum = 4.*pi*flux*angular_diameter_distance(z)**2
	lum *= (1.+z)**(3.+alpha)
	return lum/105.026

# Luminosity to Flux
#   UNITS: Jy (output, 10^21 h**-1 Watts (input)
def lum2flux(lum,z,alpha=0.7):
	flux = lum/(4.*pi*angular_diameter_distance(z)**2)
	flux /= (1.+z)**(3.+alpha)
	return flux*105.026

# Get mstar for a given filter (see accepted filters above) for a given
#   redshift. No k-corrections or evolution models are applied....
def get_mstar(filter,z):
	from math import log10
	distance = luminosity_distance(z)
	return mstar[filter] - 5. + 5.*log10(distance) + 30.
