from scipy.misc import derivative as scider
import numpy as np

def partial_derivative(func, var, point, args = [], order = 3, n = 1, h = 1.e-6, **kwargs):
    """
    Return the parial derivative of the function for variable var at point

    :param var: Index of variable to take derivative wrt. Should indicate it's position in array point.
    :param point: Value around which to take derivative.
    :param args: List of arguements required for func evaluation
    :param kwargs: Dictionary of arguements required for func evaluation
    """

    
    assert isinstance(var, int), "mypylib:numutils:derivative:partial_derivative: var must be int"
    
    pnt = point[:]
    def wraps(x):
        pnt[var] = x
        return func(pnt, *args, **kwargs)
    return scider(wraps, point[var], dx = h, order = order, n = n)


def df(*args, **kwargs):
    """
    Overloaded form of partial_derivative
    """
    return partial_derivative(*args, **kwargs)

def df_converge(func, *args, **kwargs):
    """
    Starting from h, keep taking derivatives until abs(df) is minimised
    :param args:
    :param kwargs:
    :return:
    """

    store = []
    assert "h" in kwargs, "df_converge: h must be an element of keyword arguements"

    def wrap(x):
        kwargs["h"] = x
        print("\n--- Considering step-size:", x)
        store.append(partial_derivative(func,*args, **kwargs))
        return np.abs(store[-1])

    # Call once to get typical value, to set ftol as relative
    typical = wrap(kwargs["h"])

    import scipy.optimize as opt
    min = opt.fmin(wrap,kwargs["h"], ftol = 0.001*typical, maxiter=100)

    minarg = np.argmin(store)

    return store[minarg]

def ddf(func, var, point, args, h = 1.e-6, **kwargs):
    """
    Calculate high order derivatives of func. For now, this is limited to 2nd order
    """

    import copy
    nDim = len(var)
    assert nDim == 2, "ddf has not been written for higher order derivatives"

    def d1(x):
        """
        Set up wrapping function to take in *vector* point, and return the partial derivative. For higher order derivatives, this should be stacked up to var[-1] (not inclusive)
        """
        der = partial_derivative(func, var[0], copy.copy(x), args = args, **kwargs)
        return der
    
    return partial_derivative(d1, var[1], point, h = h, **kwargs)
    
