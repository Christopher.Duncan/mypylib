import numpy as np
import mypylib.utils.numutils as numutils

def statByRadialBin(x, y, meas, nbins = 10, radLim = None, plotit = False, **kwargs):
    
    #Get radial direction
    rad = numutils.pythLineDistance(x,y)

    _tol = 1.e-6
    limit = [np.min(rad)-_tol,np.max(rad)+_tol]
    if(radLim is not None):
        limit[0] = max(limit[0], radLim[0])
        limit[1] = min(limit[1], radLim[1])
    
    bins = np.linspace(limit[0], limit[1], nbins+1)
    
    rbins = np.digitize(rad, bins)
    
    bVal = bins[:-1] + 0.5*np.diff(bins)
    average = np.zeros(bVal.shape)
    std = np.zeros(average.shape)
    meanStd = np.zeros(std.shape)
    for Bin in range(0,len(bVal)):
        bMeas = meas[rbins == Bin]

        print("For bin ", Bin, " there are ",bMeas.shape, "sources")
        
        average[Bin] = np.mean(bMeas)
        std[Bin] = np.std(bMeas)
        meanStd[Bin] = std[Bin]/np.sqrt(bMeas.shape[0])

        
        print("With average ", average[Bin], " and StD ", std[Bin])
        
    if(plotit):
        import mypylib.plot.general as plot
        plot.errorBar(bVal, average, yerr = meanStd, xlabel = "Radial Bin", plotit = plotit, **kwargs)
